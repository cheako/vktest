/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include <cassert>
#define VULKAN_HPP_NO_EXCEPTIONS 1
#define VULKAN_HPP_TYPESAFE_CONVERSION 1
#define VULKAN_HPP_NO_SMART_HANDLE 1
#define VULKAN_HPP_ASSERT my_vkassert
#define my_vkassert(expr) (__ASSERT_VOID_CAST(0))
#include <vulkan/vulkan.hpp>
#include <vulkan_extended.h>
