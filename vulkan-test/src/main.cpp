#include <iostream>
#include <set>
#include <arpa/inet.h>

#include <cassert>
#define VULKAN_HPP_NO_EXCEPTIONS 1
#define VULKAN_HPP_TYPESAFE_CONVERSION 1
#define VULKAN_HPP_NO_SMART_HANDLE 1
#define VULKAN_HPP_ASSERT my_vkassert
#define my_vkassert(expr) (__ASSERT_VOID_CAST(0))
#include <vulkan/vulkan.hpp>
// #include <vulkan_extended.h>

#include <GLFW/glfw3.h>

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

#define GLM_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "shaderc/shaderc.hpp"

// Helper function to check if the compilation result indicates a successful
// compilation.
template <typename T>
bool CompilationResultIsSuccess(const shaderc::CompilationResult<T> &result)
{
        return result.GetCompilationStatus() == shaderc_compilation_status_success;
}

bool CompileShader(const std::string name, const std::string _code, shaderc_shader_kind kind, std::vector<uint32_t> &spirv)
{
        shaderc::Compiler compiler;

        shaderc::SpvCompilationResult result = compiler.CompileGlslToSpv(_code.c_str(), _code.length(), kind, name.c_str());
        if (!CompilationResultIsSuccess(result))
        {
                std::cerr << "Failed shaderc(" << result.GetCompilationStatus() << "): " << result.GetErrorMessage() << std::endl;
                spirv.clear();

                std::cerr << "Failed shader source: " << _code << std::endl;
                return false;
        }

        spirv.assign(result.cbegin(), result.cend());

        return true;
}

constexpr struct VertexData
{
        struct VertexPos
        {
                float x;
                float y;
                float z;
                float w;
        } pos;
        struct VertexUV
        {
                float x;
                float y;
        } uv;
} s_vertices[6] = {
#if 0
    {{-1,
      -1,
      1,
      1},
     {0, 1}},
    {{-1,
      1,
      1,
      1},
     {0, 0}},
    {{1,
      1,
      1,
      1},
     {1, 0}},
    {{1,
      -1,
      1,
      1},
     {1, 1}},
    {{-1,
      -1,
      1,
      1},
     {0, 1}},
    {{1,
      1,
      1,
      1},
     {1, 0}}
#else
    {{1,
      1,
      1,
      1},
     {1, 0}},
    {{-1,
      1,
      1,
      1},
     {0, 0}},
    {{-1,
      -1,
      1,
      1},
     {0, 1}},
    {{1,
      1,
      1,
      1},
     {1, 0}},
    {{-1,
      -1,
      1,
      1},
     {0, 1}},
    {{1,
      -1,
      1,
      1},
     {1, 1}}
#endif
};

struct Vertex
{
        struct VertexData::VertexPos pos;
        struct VertexData::VertexUV uv;
        static vk::VertexInputBindingDescription const s_inputBindingDescription;
        static vk::VertexInputAttributeDescription const s_inputAttributeDescription[];
};

class Image
{
      public:
        vk::Image image;
        vk::DeviceMemory memory;
        vk::ImageView view;
};

class Buffer
{
      public:
        bool Stage(void const *data, size_t size, vk::BufferUsageFlagBits usage);

        vk::Buffer buffer;
        VmaAllocation allocation;

      private:
        bool CreateBuffer(size_t size, vk::BufferUsageFlagBits usage);

        bool CopyMemory(void const *data, size_t size);
};

class Shader
{
      public:
        bool Init(std::string name, std::string _code, vk::ShaderStageFlagBits stage);

        vk::Result state = vk::Result::eErrorInitializationFailed;
        vk::ShaderStageFlagBits stage;
        vk::ShaderModule shaderModule;
        vk::PipelineShaderStageCreateInfo shaderStage;
};

vk::VertexInputBindingDescription const Vertex::s_inputBindingDescription = {
    0, sizeof(Vertex), vk::VertexInputRate::eVertex};
vk::VertexInputAttributeDescription const Vertex::s_inputAttributeDescription[] = {{0, 0, vk::Format::eR32G32B32A32Sfloat, offsetof(Vertex, pos)}, {1, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, uv)}};

bool Buffer::Stage(void const *data, size_t size, vk::BufferUsageFlagBits usage)
{
        return CreateBuffer(size, usage) && CopyMemory(data, size);
}

vk::Device m_logical;
vk::CommandBuffer m_commandBuffer;
Buffer *m_vertexBuffer;
Shader *m_vertexShader;
Shader *m_fragmentShader;
vk::DescriptorSetLayout m_descriptorLayouts[2];
vk::DescriptorPool m_descriptorPool;
vk::PipelineLayout m_pipelineLayout;
vk::PipelineCache m_pipelineCache;
VmaAllocator m_allocator;

std::string const applicationName = "VkWayland";
uint32_t const applicationVersion = VK_MAKE_VERSION(0, 0, 1);

vk::Instance instance;
std::set<std::string> extentions;
vk::Result status = vk::Result::eErrorInitializationFailed;

GLFWwindow *data_window;
vk::SurfaceKHR surface;

uint32_t m_familyIndex;

bool Buffer::CreateBuffer(size_t size, vk::BufferUsageFlagBits usage)
{
        vk::BufferCreateInfo bufferCreateInfo;
        bufferCreateInfo.setQueueFamilyIndexCount(1);
        bufferCreateInfo.setPQueueFamilyIndices(&m_familyIndex);
        bufferCreateInfo.setSharingMode(vk::SharingMode::eExclusive);
        bufferCreateInfo.setSize(size);
        bufferCreateInfo.setUsage(usage);

        VkBufferCreateInfo tmpInfo;
        tmpInfo = bufferCreateInfo;

        VmaAllocationCreateInfo bufferAllocInfo = {};
        bufferAllocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

        VkBuffer tmpBuffer;
        status = vk::Result(vmaCreateBuffer(m_allocator, &tmpInfo, &bufferAllocInfo, &tmpBuffer, &allocation, nullptr));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create buffer." << std::endl;
                return false;
        }

        buffer = tmpBuffer;

        return true;
}

bool Buffer::CopyMemory(void const *data, size_t size)
{
        vk::Result result;
        void *mappedMemory;
        status = vk::Result(vmaMapMemory(m_allocator, allocation, &mappedMemory));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "failed to map image memory!" << std::endl;
                return false;
        }
        memcpy(mappedMemory, data, size);
        vmaUnmapMemory(m_allocator, allocation);

        vmaFlushAllocation(m_allocator, allocation, 0, size);

        return true;
}

bool Shader::Init(const std::string name, const std::string _code, vk::ShaderStageFlagBits stage)
{
        std::vector<uint32_t> shaderCode;
        if (!CompileShader(name, _code, stage == vk::ShaderStageFlagBits::eVertex ? shaderc_vertex_shader : shaderc_fragment_shader, shaderCode))
        {
                std::cerr << "Failed to compile shader: " << name << std::endl;
                return false;
        }

        vk::ShaderModuleCreateInfo shaderModuleCreateInfo;
        shaderModuleCreateInfo.setCodeSize(static_cast<size_t>(shaderCode.size() * sizeof(int)));
        shaderModuleCreateInfo.setPCode(reinterpret_cast<uint32_t *>(shaderCode.data()));
        std::tie(state, shaderModule) = m_logical.createShaderModule(shaderModuleCreateInfo);
        if (state != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create shader module: " << name << std::endl;
                return false;
        }

        shaderStage.setPName("main");
        shaderStage.setStage(stage);
        shaderStage.setModule(shaderModule);

        return true;
}

vk::PhysicalDevice m_physical;
std::set<std::string> m_device_extentions;
vk::Queue m_queue;
vk::CommandPool m_commandPool;
vk::Fence m_fence;
vk::Semaphore m_renderDoneSemaphore;
vk::Sampler m_sampler;

vk::Image m_textureImage;
int32_t m_offset;
vk::Buffer m_buffer;
vk::ImageView m_textureImageView;
vk::Buffer m_uniformBuffers[2];
vk::DescriptorSet m_descriptorSet[2];
VmaAllocation m_allocation;
VmaAllocation m_uniformAllocations[2];
VmaAllocation m_textureAllocation;

vk::Semaphore m_imageAvailableSemaphore;
vk::SwapchainKHR m_swapchain;
uint32_t m_currentFrameBuffer;
class NextImage
{
      public:
        vk::Image m_image;
        vk::ImageView m_imageView;
        vk::Framebuffer m_framebuffer;
};
std::vector<NextImage> m_nextImages;
// uint32_t const m_framebufferCount = 2;
vk::RenderPass m_renderPass;
vk::Pipeline m_pipeline;

bool SFrame()
{
        std::tie(status, m_currentFrameBuffer) = m_logical.acquireNextImageKHR(
            m_swapchain, UINT64_MAX, m_imageAvailableSemaphore, nullptr);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to acquire framebuffer image." << std::endl;
                return false;
        }

        vk::RenderPassBeginInfo renderPassBegin;
        renderPassBegin.setFramebuffer(m_nextImages[m_currentFrameBuffer].m_framebuffer);
        renderPassBegin.setRenderArea(vk::Rect2D({0, 0}, {800, 600}));
        renderPassBegin.setRenderPass(m_renderPass);
        m_commandBuffer.beginRenderPass(renderPassBegin, vk::SubpassContents::eInline);
        m_commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_pipeline);
        m_commandBuffer.bindVertexBuffers(0, m_vertexBuffer->buffer, vk::DeviceSize(0));

        m_commandBuffer.bindDescriptorSets(
            vk::PipelineBindPoint::eGraphics,
            m_pipelineLayout, 1,
            m_descriptorSet[1], nullptr);

        m_commandBuffer.bindDescriptorSets(
            vk::PipelineBindPoint::eGraphics,
            m_pipelineLayout, 0,
            m_descriptorSet[0], nullptr);
        m_commandBuffer.draw(6, 1, 0, 0);

        m_commandBuffer.endRenderPass();

        return true;
}

bool CreatePipeline()
{
        vk::PipelineVertexInputStateCreateInfo vertexInputCreateInfo;
        vertexInputCreateInfo.setVertexAttributeDescriptionCount(2);
        vertexInputCreateInfo.setPVertexAttributeDescriptions(Vertex::s_inputAttributeDescription);
        vertexInputCreateInfo.setVertexBindingDescriptionCount(1);
        vertexInputCreateInfo.setPVertexBindingDescriptions(&Vertex::s_inputBindingDescription);

        vk::PipelineInputAssemblyStateCreateInfo inputAssemblyCreateInfo;
        inputAssemblyCreateInfo.setTopology(vk::PrimitiveTopology::eTriangleList);
        inputAssemblyCreateInfo.setPrimitiveRestartEnable(false);

        vk::PipelineShaderStageCreateInfo const shaderStages[2] = {
            m_vertexShader->shaderStage, m_fragmentShader->shaderStage};

        vk::Viewport viewport;
        viewport.setWidth(800);
        viewport.setHeight(600);
        viewport.setX(0);
        viewport.setY(0);
        viewport.setMaxDepth(1.0f);
        viewport.setMinDepth(0);

        vk::Rect2D scissor;
        scissor.setOffset({0, 0});
        scissor.setExtent({800, 600});

        vk::PipelineViewportStateCreateInfo viewportCreateInfo;
        viewportCreateInfo.setScissorCount(1);
        viewportCreateInfo.setPScissors(&scissor);
        viewportCreateInfo.setViewportCount(1);
        viewportCreateInfo.setPViewports(&viewport);

        vk::PipelineRasterizationStateCreateInfo rasterizationCreateInfo;
        rasterizationCreateInfo.setCullMode(vk::CullModeFlagBits::eNone);
        rasterizationCreateInfo.setPolygonMode(vk::PolygonMode::eFill);
        rasterizationCreateInfo.setFrontFace(vk::FrontFace::eClockwise);
        rasterizationCreateInfo.setLineWidth(1.0f);
        rasterizationCreateInfo.setDepthClampEnable(false);
        rasterizationCreateInfo.setDepthBiasEnable(false);
        rasterizationCreateInfo.setRasterizerDiscardEnable(false);

        vk::PipelineMultisampleStateCreateInfo multisamplingCreateInfo;
        multisamplingCreateInfo.setSampleShadingEnable(false);
        multisamplingCreateInfo.setRasterizationSamples(vk::SampleCountFlagBits::e1);

        vk::PipelineColorBlendAttachmentState colorBlendAttachmentState;
        colorBlendAttachmentState.setBlendEnable(false);
        colorBlendAttachmentState.setColorWriteMask(
            vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA);

        vk::PipelineColorBlendStateCreateInfo colorBlendCreateInfo;
        colorBlendCreateInfo.setAttachmentCount(1);
        colorBlendCreateInfo.setPAttachments(&colorBlendAttachmentState);
        colorBlendCreateInfo.setLogicOpEnable(false);

        vk::GraphicsPipelineCreateInfo pipelineCreateInfo;
        pipelineCreateInfo.setPVertexInputState(&vertexInputCreateInfo);
        pipelineCreateInfo.setPInputAssemblyState(&inputAssemblyCreateInfo);
        pipelineCreateInfo.setStageCount(2);
        pipelineCreateInfo.setPStages(shaderStages);
        pipelineCreateInfo.setPViewportState(&viewportCreateInfo);
        pipelineCreateInfo.setPRasterizationState(&rasterizationCreateInfo);
        pipelineCreateInfo.setPMultisampleState(&multisamplingCreateInfo);
        pipelineCreateInfo.setPColorBlendState(&colorBlendCreateInfo);
        pipelineCreateInfo.setRenderPass(m_renderPass);
        pipelineCreateInfo.setSubpass(0);
        pipelineCreateInfo.setLayout(m_pipelineLayout);

        std::tie(status, m_pipeline) = m_logical.createGraphicsPipeline(m_pipelineCache, pipelineCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create graphics pipeline." << std::endl;
                return false;
        }

        return true;
}

bool CreateDescriptorSetOne()
{
        vk::DescriptorSetAllocateInfo allocInfo;
        allocInfo.setDescriptorPool(m_descriptorPool);
        allocInfo.setDescriptorSetCount(1);
        allocInfo.setPSetLayouts(&m_descriptorLayouts[1]);

        std::vector<vk::DescriptorSet> sets;
        std::tie(status, sets) = m_logical.allocateDescriptorSets(allocInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create render done semaphore." << std::endl;
                return false;
        }

        m_descriptorSet[1] = sets[0];

        return true;
}

bool CreateUniformBufferOne()
{
        vk::BufferCreateInfo bufferInfo;
        bufferInfo.setSize(sizeof(glm::mat4));
        bufferInfo.setUsage(vk::BufferUsageFlagBits::eUniformBuffer);

        VkBufferCreateInfo tmpInfo;
        tmpInfo = bufferInfo;

        VmaAllocationCreateInfo allocCreateInfo = {};
        allocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        allocCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        VkBuffer tmpBuffer;
        status = vk::Result(vmaCreateBuffer(m_allocator, &tmpInfo, &allocCreateInfo, &tmpBuffer, &m_uniformAllocations[1], nullptr));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create uniform buffer." << std::endl;
                return false;
        }

        m_uniformBuffers[1] = tmpBuffer;

        auto projMat = glm::ortho(0.f, 800.f, 0.f, 600.f, -1.f, 1.f);
        auto viewMat = glm::lookAt(glm::vec3(400.f, 300.f, -1.f), glm::vec3(400.f, 300.f, 0.f), glm::vec3(400.f, 600.f, -1.f));
        glm::mat4 *uniform;

        vmaMapMemory(m_allocator, m_uniformAllocations[1], reinterpret_cast<void **>(&uniform));
        *uniform = projMat * viewMat;
        vmaUnmapMemory(m_allocator, m_uniformAllocations[1]);

        vk::DescriptorBufferInfo descriptorInfo;
        descriptorInfo.setBuffer(m_uniformBuffers[1]);
        descriptorInfo.setRange(sizeof(glm::mat4));

        vk::WriteDescriptorSet writeInfo;
        writeInfo.setDescriptorType(vk::DescriptorType::eUniformBuffer);
        writeInfo.setDescriptorCount(1);
        writeInfo.setPBufferInfo(&descriptorInfo);
        writeInfo.setDstSet(m_descriptorSet[1]);
        m_logical.updateDescriptorSets(writeInfo, nullptr);

        return true;
}

bool CreateRenderPass()
{
        vk::AttachmentDescription attachmentDescription;
        attachmentDescription.setFormat(vk::Format::eB8G8R8A8Srgb);
        attachmentDescription.setSamples(vk::SampleCountFlagBits::e1);
        attachmentDescription.setInitialLayout(vk::ImageLayout::eUndefined);
        attachmentDescription.setFinalLayout(vk::ImageLayout::ePresentSrcKHR);
        attachmentDescription.setLoadOp(vk::AttachmentLoadOp::eLoad);
        attachmentDescription.setStoreOp(vk::AttachmentStoreOp::eStore);
        attachmentDescription.setStencilLoadOp(vk::AttachmentLoadOp::eDontCare);
        attachmentDescription.setStencilStoreOp(vk::AttachmentStoreOp::eDontCare);

        vk::AttachmentReference attachmentReference;
        attachmentReference.setAttachment(0);
        attachmentReference.setLayout(vk::ImageLayout::eColorAttachmentOptimal);

        vk::SubpassDescription subpass;
        subpass.setColorAttachmentCount(1);
        subpass.setPColorAttachments(&attachmentReference);
        subpass.setPipelineBindPoint(vk::PipelineBindPoint::eGraphics);

        vk::RenderPassCreateInfo renderPassCreateInfo;
        renderPassCreateInfo.setAttachmentCount(1);
        renderPassCreateInfo.setPAttachments(&attachmentDescription);
        renderPassCreateInfo.setSubpassCount(1);
        renderPassCreateInfo.setPSubpasses(&subpass);

        std::tie(status, m_renderPass) = m_logical.createRenderPass(renderPassCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create renderpass." << std::endl;
                return false;
        }

        return true;
}

bool CreateSwapchainImageViews()
{
        std::vector<vk::Image> images;
        std::tie(status, images) = m_logical.getSwapchainImagesKHR(m_swapchain);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get swapchain images." << std::endl;
                return false;
        }

        m_nextImages.resize(images.size());
        for (size_t i = 0; i < images.size(); ++i)
        {
                m_nextImages[i].m_image = images[i];

                vk::ImageSubresourceRange range;
                range.setLayerCount(1);
                range.setLevelCount(1);
                range.setAspectMask(vk::ImageAspectFlagBits::eColor);

                vk::ImageViewCreateInfo imageViewCreateInfo;
                imageViewCreateInfo.setFormat(vk::Format::eB8G8R8A8Srgb);
                imageViewCreateInfo.setImage(images[i]);
                imageViewCreateInfo.setViewType(vk::ImageViewType::e2D);
                imageViewCreateInfo.setSubresourceRange(range);
                imageViewCreateInfo.setComponents({vk::ComponentSwizzle::eR, vk::ComponentSwizzle::eG, vk::ComponentSwizzle::eB, vk::ComponentSwizzle::eA});

                std::tie(status, m_nextImages[i].m_imageView) = m_logical.createImageView(imageViewCreateInfo);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create swapchain image view." << std::endl;
                        return false;
                }

                vk::FramebufferCreateInfo framebufferCreateInfo;
                framebufferCreateInfo.setRenderPass(m_renderPass);
                framebufferCreateInfo.setAttachmentCount(1);
                framebufferCreateInfo.setPAttachments(&m_nextImages[i].m_imageView);
                framebufferCreateInfo.setWidth(800);
                framebufferCreateInfo.setHeight(600);
                framebufferCreateInfo.setLayers(1);

                std::tie(status, m_nextImages[i].m_framebuffer) = m_logical.createFramebuffer(framebufferCreateInfo);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create framebuffer." << std::endl;
                        return false;
                }
        }

        return true;
}

const static vk::ImageSubresourceRange subresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

bool Frame()
{
        status = m_logical.waitIdle();
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to go idle." << std::endl;
                return false;
        }

        vk::CommandBufferBeginInfo beginInfo;
        beginInfo.setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
        m_commandBuffer.begin(beginInfo);

        {
#include "image_info.h"
#include "image_data.h"
                static bool init = true;
                if (init)
                {
                        init = false;

                        m_offset = reinterpret_cast<uintptr_t>(image_data_0DyGJS) % 32;
                        vk::BufferCreateInfo bufferInfo;
                        bufferInfo.setSize(stride * height + m_offset);
                        bufferInfo.setUsage(vk::BufferUsageFlagBits::eTransferSrc);
                        bufferInfo.setSharingMode(vk::SharingMode::eExclusive);

                        VkBufferCreateInfo tmpInfo;
                        tmpInfo = bufferInfo;

                        VmaAllocationCreateInfo bufferAllocInfo = {};
                        bufferAllocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

                        VkBuffer tmpBuffer;
                        status = vk::Result(vmaCreateBuffer(m_allocator, &tmpInfo, &bufferAllocInfo, &tmpBuffer, &m_allocation, nullptr));
                        if (status != vk::Result::eSuccess)
                        {
                                std::cerr << "Failed to create uniform buffer." << std::endl;
                        }

                        m_buffer = tmpBuffer;

                        vk::ImageCreateInfo imageInfo;
                        imageInfo.setImageType(vk::ImageType::e2D);
                        imageInfo.setExtent(vk::Extent3D(width, height, 1));
                        imageInfo.setMipLevels(1);
                        imageInfo.setArrayLayers(1);

                        imageInfo.setFormat(vk::Format(format));
                        imageInfo.setTiling(vk::ImageTiling::eLinear);
                        imageInfo.setInitialLayout(vk::ImageLayout::eUndefined);
                        imageInfo.setUsage(vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled);
                        imageInfo.setSamples(vk::SampleCountFlagBits::e1);

                        VkImageCreateInfo tmpImageInfo;
                        tmpImageInfo = imageInfo;

                        VmaAllocationCreateInfo allocInfo = {};
                        allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                        VkImage imageTemp;
                        status = vk::Result(vmaCreateImage(
                            m_allocator,
                            &tmpImageInfo, &allocInfo, &imageTemp,
                            &m_textureAllocation, nullptr));
                        if (status != vk::Result::eSuccess)
                        {
                                throw std::runtime_error("failed to create image!");
                        }

                        m_textureImage = imageTemp;

                        vk::ImageViewCreateInfo viewInfo;
                        viewInfo.setImage(m_textureImage);
                        viewInfo.setViewType(vk::ImageViewType::e2D);
                        viewInfo.setFormat(vk::Format(format));
                        viewInfo.setSubresourceRange(subresourceRange);

                        std::tie(status, m_textureImageView) = m_logical.createImageView(viewInfo);
                        if (status != vk::Result::eSuccess)
                        {
                                throw std::runtime_error("failed to allocate texture view!");
                        }

                        vk::DescriptorImageInfo descriptorImageInfo;
                        descriptorImageInfo.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
                        descriptorImageInfo.setImageView(m_textureImageView);
                        descriptorImageInfo.setSampler(m_sampler);

                        vk::WriteDescriptorSet writeInfo;
                        writeInfo.setDescriptorCount(1);
                        writeInfo.setDstBinding(1);
                        writeInfo.setDescriptorType(vk::DescriptorType::eCombinedImageSampler);
                        writeInfo.setPImageInfo(&descriptorImageInfo);
                        writeInfo.setDstSet(m_descriptorSet[0]);
                        m_logical.updateDescriptorSets(writeInfo, nullptr);

                        char *buffer_data;
                        status = vk::Result(vmaMapMemory(m_allocator, m_allocation, reinterpret_cast<void **>(&buffer_data)));
                        if (status != vk::Result::eSuccess)
                        {
                                throw std::runtime_error("failed to map image memory!");
                        }

#if 1
                        memcpy(buffer_data + m_offset, image_data_0DyGJS, image_data_0DyGJS_len);
#else
                        // memset(buffer_data + m_offset, 0x88, image_data_0DyGJS_len);
                        for(
                                uint32_t *i = reinterpret_cast<uint32_t*>(buffer_data + m_offset);
                                reinterpret_cast<uintptr_t>(i) < buffer_data + m_offset + image_data_0DyGJS_len;
                                ++i)
                                *i = htonl((reinterpret_cast<uintptr_t>(i) - buffer_data + m_offset) | 0xff000000);
                                
#endif
                        vmaUnmapMemory(m_allocator, m_allocation);

                        vmaFlushAllocation(m_allocator, m_allocation, 0, stride * height);

#if 1
                }
#endif

                vk::ImageMemoryBarrier barrier;
                barrier.setOldLayout(vk::ImageLayout::eUndefined);
                barrier.setNewLayout(vk::ImageLayout::eTransferDstOptimal);
                barrier.setDstAccessMask(vk::AccessFlagBits::eTransferWrite);
                barrier.setSubresourceRange(subresourceRange);

                barrier.setImage(m_textureImage);

                m_commandBuffer.pipelineBarrier(
                    vk::PipelineStageFlagBits::eTopOfPipe,
                    vk::PipelineStageFlagBits::eTransfer,
                    (vk::DependencyFlags)0,
                    nullptr, nullptr, barrier);

                const vk::ImageSubresourceLayers subresourceLayers(vk::ImageAspectFlagBits::eColor, 0, 0, 1);

                vk::BufferImageCopy region;
                region.setImageOffset(m_offset);
                region.setBufferRowLength(stride / 4);
                region.setImageSubresource(subresourceLayers);

                region.setImageOffset(vk::Offset3D(0, 0, 0));
                region.setImageExtent(vk::Extent3D(
                    width,
                    height,
                    1));

                m_commandBuffer.copyBufferToImage(
                    m_buffer,
                    m_textureImage,
                    vk::ImageLayout::eTransferDstOptimal,
                    region);

                barrier.setOldLayout(vk::ImageLayout::eTransferDstOptimal);
                barrier.setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
                barrier.setSrcAccessMask(vk::AccessFlagBits::eTransferWrite);
                barrier.setDstAccessMask(vk::AccessFlagBits::eShaderRead);

                m_commandBuffer.pipelineBarrier(
                    vk::PipelineStageFlagBits::eTransfer,
                    vk::PipelineStageFlagBits::eFragmentShader,
                    (vk::DependencyFlags)0,
                    nullptr, nullptr,
                    barrier);
#if 0
        }
#endif
        }

        bool ret;
        ret = SFrame();

        m_commandBuffer.end();

        vk::SubmitInfo submitInfo;
        submitInfo.setCommandBufferCount(1);
        submitInfo.setPCommandBuffers(&m_commandBuffer);
        vk::PipelineStageFlags waitStage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
        submitInfo.setPWaitDstStageMask(&waitStage);
        submitInfo.setWaitSemaphoreCount(1);
        submitInfo.setPWaitSemaphores(&m_imageAvailableSemaphore);
        submitInfo.setSignalSemaphoreCount(1);
        submitInfo.setPSignalSemaphores(&m_renderDoneSemaphore);

        status = m_queue.submit(1, &submitInfo, m_fence);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to submit cmd." << std::endl;
                return false;
        }

        while (m_logical.waitForFences(vk::ArrayProxy<const vk::Fence>(m_fence), true, UINT64_MAX) == vk::Result::eTimeout)
                ;
        m_logical.resetFences(vk::ArrayProxy<const vk::Fence>(m_fence));

        vk::PresentInfoKHR presentInfo;
        presentInfo.setWaitSemaphoreCount(1);
        presentInfo.setPWaitSemaphores(&m_renderDoneSemaphore);
        presentInfo.setSwapchainCount(1);
        presentInfo.setPSwapchains(&m_swapchain);
        presentInfo.setPImageIndices(&m_currentFrameBuffer);
        status = m_queue.presentKHR(presentInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to present queue." << std::endl;
                return false;
        }

        return ret;
}

bool GetSurfaceSupportKHR()
{
        vk::Bool32 supported;
        std::tie(status, supported) = m_physical.getSurfaceSupportKHR(m_familyIndex, surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface support." << std::endl;
                return false;
        }
        return supported;
}

bool CreateSemaphore()
{
        vk::SemaphoreCreateInfo createInfo;
        std::tie(status, m_renderDoneSemaphore) = m_logical.createSemaphore(createInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create render done semaphore." << std::endl;
                return false;
        }
        return true;
}

bool CreateFence()
{
        vk::FenceCreateInfo createInfo;
        std::tie(status, m_fence) = m_logical.createFence(createInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create fence." << std::endl;
                return false;
        }
        return true;
}

bool CreateSurfaceSwapchain()
{
        vk::SurfaceCapabilitiesKHR capabilities;
        std::vector<vk::SurfaceFormatKHR> imageFormats;
        std::vector<vk::PresentModeKHR> presentModes;

        std::tie(status, capabilities) = m_physical.getSurfaceCapabilitiesKHR(surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface capabilities." << std::endl;
                return false;
        }

        std::tie(status, imageFormats) = m_physical.getSurfaceFormatsKHR(surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface image formats." << std::endl;
                return false;
        }

        std::tie(status, presentModes) = m_physical.getSurfacePresentModesKHR(surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface present modes." << std::endl;
                return false;
        }

        if (!(capabilities.supportedCompositeAlpha & vk::CompositeAlphaFlagBitsKHR::eOpaque))
        {
                std::cerr << "Surface does not support opaque composite alpha." << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        if (capabilities.maxImageCount != 0 && capabilities.maxImageCount < 2)
        {
                std::cerr << "Surface does not support double buffering." << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }
        if (std::find(imageFormats.begin(), imageFormats.end(), (vk::SurfaceFormatKHR){vk::Format::eB8G8R8A8Srgb, vk::ColorSpaceKHR::eSrgbNonlinear}) == imageFormats.end())
        {
                std::cerr << "Device does not support image format." << std::endl;
                for (size_t i = 0; i < imageFormats.size(); ++i)
                        std::cerr << "Format(" << i << "): " << static_cast<uint32_t>(imageFormats[i].format) << ", " << static_cast<uint32_t>(imageFormats[i].colorSpace) << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        vk::PresentModeKHR presentMode = vk::PresentModeKHR::eFifo;
        if (std::find(presentModes.begin(), presentModes.end(), vk::PresentModeKHR::eFifo) == presentModes.end())
        {
                std::cerr << "Device does not support fifo presetn mode." << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        vk::SwapchainCreateInfoKHR swapchainCreateInfo;
        swapchainCreateInfo.setSurface(surface);
        swapchainCreateInfo.setMinImageCount(2);
        swapchainCreateInfo.setImageFormat(vk::Format::eB8G8R8A8Srgb);
        swapchainCreateInfo.setImageColorSpace(vk::ColorSpaceKHR::eSrgbNonlinear);
        swapchainCreateInfo.setImageExtent(vk::Extent2D(800, 600));
        swapchainCreateInfo.setImageArrayLayers(1);
        swapchainCreateInfo.setImageUsage(vk::ImageUsageFlagBits::eColorAttachment);
        swapchainCreateInfo.setImageSharingMode(vk::SharingMode::eExclusive);
        swapchainCreateInfo.setQueueFamilyIndexCount(1);
        swapchainCreateInfo.setPQueueFamilyIndices(&m_familyIndex);
        swapchainCreateInfo.setPreTransform(vk::SurfaceTransformFlagBitsKHR::eIdentity);
        swapchainCreateInfo.setCompositeAlpha(vk::CompositeAlphaFlagBitsKHR::eOpaque);
        swapchainCreateInfo.setPresentMode(presentMode);
        swapchainCreateInfo.setClipped(true);

        std::tie(status, m_swapchain) = m_logical.createSwapchainKHR(swapchainCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create swapchain" << std::endl;
                return false;
        }

        return true;
}

bool CreateSurfaceSemaphore()
{
        vk::SemaphoreCreateInfo createInfo;

        std::tie(status, m_imageAvailableSemaphore) = m_logical.createSemaphore(createInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create framebuffer." << std::endl;
                return false;
        }

        return true;
}

bool CreateSampler()
{
        vk::SamplerCreateInfo samplerInfo;
#if 1
        samplerInfo.setMagFilter(vk::Filter::eNearest);
        samplerInfo.setMinFilter(vk::Filter::eNearest);
        samplerInfo.setAddressModeU(vk::SamplerAddressMode::eClampToEdge);
        samplerInfo.setAddressModeV(vk::SamplerAddressMode::eClampToEdge);
        samplerInfo.setAddressModeW(vk::SamplerAddressMode::eClampToEdge);
        samplerInfo.setBorderColor(vk::BorderColor::eIntOpaqueBlack);
        samplerInfo.setCompareOp(vk::CompareOp::eAlways);
        samplerInfo.setMipmapMode(vk::SamplerMipmapMode::eNearest);
#else
                samplerInfo.setMagFilter(vk::Filter::eLinear);
                samplerInfo.setMinFilter(vk::Filter::eLinear);
                samplerInfo.setAddressModeU(vk::SamplerAddressMode::eRepeat);
                samplerInfo.setAddressModeV(vk::SamplerAddressMode::eRepeat);
                samplerInfo.setAddressModeW(vk::SamplerAddressMode::eRepeat);
                samplerInfo.setAnisotropyEnable(VK_FALSE);
                samplerInfo.setMaxAnisotropy(1);
                samplerInfo.setUnnormalizedCoordinates(VK_FALSE);
                samplerInfo.setCompareEnable(VK_FALSE);
                samplerInfo.setMipLodBias(0.0f);
                samplerInfo.setMinLod(0.0f);
                samplerInfo.setMaxLod(0.0f);
                samplerInfo.setBorderColor(vk::BorderColor::eIntOpaqueWhite);
                samplerInfo.setCompareOp(vk::CompareOp::eAlways);
                samplerInfo.setMipmapMode(vk::SamplerMipmapMode::eLinear);
#endif

        std::tie(status, m_sampler) = m_logical.createSampler(samplerInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create framebuffer." << std::endl;
                return false;
        }

        return true;
}

bool CreateShaders()
{
        m_vertexShader = new Shader();
        m_fragmentShader = new Shader();
        return m_vertexShader->Init("main_vertex",
                                    R"(#version 450

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec2 inUV;

layout(set = 1, binding = 0) uniform SUBO {
  mat4 projView;
} display;

layout(set = 0, binding = 0) uniform UBO {
  mat4 model;
} surface;

layout(location = 0) out vec2 outUV;

void main()
{
    outUV = inUV;
    /* Trash */
    //gl_Position = display.projView * surface.model * inPosition;
    /* Full screen */
    gl_Position = inPosition;
}
)",
                                    vk::ShaderStageFlagBits::eVertex) &&
               m_fragmentShader->Init("main_fragment",
                                      R"(#version 450

layout(location = 0) in vec2 inUV;

layout(set = 0, binding = 1) uniform sampler2D surface;

layout(location = 0) out vec4 outColor;

void main()
{
    /* Works, solid color */
    //outColor = vec4(1.0, 0, 0.7, 1.0);
    /* Black */
    outColor = texture(surface, inUV);
    /* Still Black */
    //outColor.rgb = texture(surface, inUV).rgb;
    //outColor.a = 1.0;
    /* Should work */
    //outColor = vec4(inUV, 0.2, 1.0);
}
)",
                                      vk::ShaderStageFlagBits::eFragment);
}

bool CreateVMA()
{
        VmaAllocatorCreateInfo allocatorInfo = {};
        allocatorInfo.physicalDevice = m_physical;
        allocatorInfo.device = m_logical;
        status = vk::Result(vmaCreateAllocator(&allocatorInfo, &m_allocator));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create allocator." << std::endl;
                return false;
        }

        return true;
}

bool CreateDescriptorPool()
{
        std::vector<vk::DescriptorPoolSize> poolSizes = {
            {vk::DescriptorType::eSampler, 4096},
            {vk::DescriptorType::eCombinedImageSampler, 4096},
            {vk::DescriptorType::eSampledImage, 4096},
            {vk::DescriptorType::eStorageImage, 4096},
            {vk::DescriptorType::eUniformTexelBuffer, 4096},
            {vk::DescriptorType::eStorageTexelBuffer, 4096},
            {vk::DescriptorType::eUniformBuffer, 4096},
            {vk::DescriptorType::eStorageBuffer, 4096},
            {vk::DescriptorType::eUniformBufferDynamic, 4096},
            {vk::DescriptorType::eStorageBufferDynamic, 4096},
            {vk::DescriptorType::eInputAttachment, 4096}};

        vk::DescriptorPoolCreateInfo poolInfo;
        poolInfo.setPoolSizeCount(poolSizes.size());
        poolInfo.setPPoolSizes(poolSizes.data());
        poolInfo.setMaxSets(12 * 4096);
        poolInfo.setFlags(vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet);

        std::tie(status, m_descriptorPool) = m_logical.createDescriptorPool(poolInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create descriptor pool." << std::endl;
                return false;
        }

        return true;
}

bool CreateDescriptorLayout()
{
        std::vector<vk::DescriptorSetLayoutBinding> layoutBindings;
        layoutBindings.insert(layoutBindings.end(),
                              vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex));
        layoutBindings.insert(layoutBindings.end(),
                              vk::DescriptorSetLayoutBinding(1, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment));

        vk::DescriptorSetLayoutCreateInfo layoutInfo;
        layoutInfo.setBindingCount(layoutBindings.size());
        layoutInfo.setPBindings(layoutBindings.data());

        std::tie(status, m_descriptorLayouts[0]) = m_logical.createDescriptorSetLayout(layoutInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create descriptor layout." << std::endl;
                return false;
        }

        layoutBindings = {};
        layoutBindings.insert(layoutBindings.end(),
                              vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex));

        layoutInfo.setBindingCount(layoutBindings.size());
        layoutInfo.setPBindings(layoutBindings.data());

        std::tie(status, m_descriptorLayouts[1]) = m_logical.createDescriptorSetLayout(layoutInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create descriptor layout." << std::endl;
                return false;
        }

        return true;
}

bool CreatePipelineLayout()
{
        vk::PipelineLayoutCreateInfo layoutCreateInfo;
        layoutCreateInfo.setSetLayoutCount(2);
        layoutCreateInfo.setPSetLayouts(m_descriptorLayouts);

        std::tie(status, m_pipelineLayout) = m_logical.createPipelineLayout(layoutCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create pipeline layout." << std::endl;
                return false;
        }

        vk::PipelineCacheCreateInfo cacheCreateInfo;
        std::tie(status, m_pipelineCache) = m_logical.createPipelineCache(cacheCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create pipeline cache." << std::endl;
                return false;
        }

        return true;
}

bool CreateVertexBuffer()
{
        m_vertexBuffer = new Buffer();
        return m_vertexBuffer->Stage(s_vertices, sizeof(s_vertices), vk::BufferUsageFlagBits::eVertexBuffer);
}

bool CreateCommandPool()
{
        vk::CommandPoolCreateInfo cmdPoolCreateInfo;
        cmdPoolCreateInfo.setFlags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer);
        cmdPoolCreateInfo.setQueueFamilyIndex(m_familyIndex);

        std::tie(status, m_commandPool) = m_logical.createCommandPool(cmdPoolCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create command pool." << std::endl;
                return false;
        }

        return true;
}

bool CreatePrimaryBuffer()
{
        vk::CommandBufferAllocateInfo cmdAllocInfo;
        cmdAllocInfo.setCommandBufferCount(1);
        cmdAllocInfo.setCommandPool(m_commandPool);
        cmdAllocInfo.setLevel(vk::CommandBufferLevel::ePrimary);

        std::vector<vk::CommandBuffer> storage;
        std::tie(status, storage) = m_logical.allocateCommandBuffers(cmdAllocInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create primary command buffer." << std::endl;
                return false;
        }
        m_commandBuffer = storage.front();

        return true;
}

bool CreateDescriptorSetZero()
{
        vk::DescriptorSetAllocateInfo allocInfo;
        allocInfo.setDescriptorPool(m_descriptorPool);
        allocInfo.setDescriptorSetCount(1);
        allocInfo.setPSetLayouts(&m_descriptorLayouts[0]);

        std::vector<vk::DescriptorSet> sets;
        std::tie(status, sets) = m_logical.allocateDescriptorSets(allocInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create render done semaphore." << std::endl;
                return false;
        }

        m_descriptorSet[0] = sets[0];

        return true;
}

bool CreateUniformBuffer()
{
        vk::BufferCreateInfo bufferInfo;
        bufferInfo.setSize(sizeof(glm::mat4));
        bufferInfo.setUsage(vk::BufferUsageFlagBits::eUniformBuffer);

        VkBufferCreateInfo tmpInfo;
        tmpInfo = bufferInfo;

        VmaAllocationCreateInfo bufferAllocInfo = {};
        bufferAllocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        bufferAllocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        VkBuffer tmpBuffer;
        status = vk::Result(vmaCreateBuffer(m_allocator, &tmpInfo, &bufferAllocInfo, &tmpBuffer, &m_uniformAllocations[0], nullptr));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create uniform buffer." << std::endl;
                return false;
        }

        m_uniformBuffers[0] = tmpBuffer;

        glm::mat4 *uniform;
        vmaMapMemory(m_allocator, m_uniformAllocations[0], reinterpret_cast<void **>(&uniform));
        *uniform = glm::mat4();
        vmaUnmapMemory(m_allocator, m_uniformAllocations[0]);

        vk::DescriptorBufferInfo descriptorBufferInfo;
        descriptorBufferInfo.setBuffer(m_uniformBuffers[0]);
        descriptorBufferInfo.setRange(sizeof(glm::mat4));

        vk::WriteDescriptorSet writeInfo;
        writeInfo.setDescriptorCount(1);
        writeInfo.setDstSet(m_descriptorSet[0]);
        writeInfo.setDescriptorType(vk::DescriptorType::eUniformBuffer);
        writeInfo.setPBufferInfo(&descriptorBufferInfo);
        m_logical.updateDescriptorSets(writeInfo, nullptr);

        return true;
}

bool CreateInstance()
{
        glfwSetErrorCallback([](int error, const char *description) {
                fprintf(stderr, "Error %d: %s\n", error, description);
        });

        if(!glfwInit())
        {
                std::cerr << "glfwInit() not right" << std::endl;
                return false;
        }

        if (!glfwVulkanSupported())
        {
                std::cerr << "Vulkan is not supported" << std::endl;
                return false;
        }

        uint32_t glfwExtCount = 0;
        const char **glfwExts = glfwGetRequiredInstanceExtensions(&glfwExtCount);
        std::vector<char const *> enabledInstanceExtensions(glfwExts, glfwExts + glfwExtCount);
        std::vector<char const *> enabledInstanceLayers{};
#ifndef NDEBUG
        enabledInstanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
        enabledInstanceLayers.push_back("VK_LAYER_LUNARG_standard_validation");
#endif

        vk::ApplicationInfo appInfo;
        appInfo.setApiVersion(VK_API_VERSION_1_1);
        appInfo.setApplicationVersion(applicationVersion);
        appInfo.setPApplicationName(applicationName.c_str());
        appInfo.setPEngineName("VkWaylandGLFW");
        appInfo.setEngineVersion(VK_MAKE_VERSION(0, 0, 1));

        vk::InstanceCreateInfo instanceCreateInfo;
        instanceCreateInfo.setPApplicationInfo(&appInfo);
        instanceCreateInfo.setEnabledLayerCount(static_cast<uint32_t>(enabledInstanceLayers.size()));
        instanceCreateInfo.setPpEnabledLayerNames(enabledInstanceLayers.data());
        instanceCreateInfo.setEnabledExtensionCount(static_cast<uint32_t>(enabledInstanceExtensions.size()));
        instanceCreateInfo.setPpEnabledExtensionNames(enabledInstanceExtensions.data());

        std::tie(status, instance) = vk::createInstance(instanceCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create Vulkan instance." << std::endl;
                return false;
        }

        return true;
}

bool CreateSurface()
{
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
        data_window = glfwCreateWindow(800, 600, applicationName.c_str(), NULL, NULL);

        VkSurfaceKHR _surface;
        status = vk::Result(glfwCreateWindowSurface(instance, data_window, NULL, &_surface));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "glfwCreateWindowSurface: Failed." << std::endl;
                return false;
        }

        surface = vk::SurfaceKHR(_surface);

        return true;
}

bool CreateDevice()
{
        m_queue = m_logical.getQueue(m_familyIndex, 0);
        return CreateSemaphore() &&
               CreateFence() &&
               CreateSampler() &&
               CreateShaders() &&
               CreateVMA() &&
               CreateDescriptorPool() &&
               CreateDescriptorLayout() &&
               CreatePipelineLayout() &&
               CreateVertexBuffer() &&
               CreateCommandPool() &&
               CreatePrimaryBuffer() &&
               CreateDescriptorSetZero() &&
               CreateUniformBuffer() &&
               true;
}

bool CreateLogicalDevice()
{
        vk::DeviceQueueCreateInfo deviceQueueCreateInfo;
        float const priority = 1.0f;
        deviceQueueCreateInfo.setPQueuePriorities(&priority);
        deviceQueueCreateInfo.setQueueCount(1);
        deviceQueueCreateInfo.setQueueFamilyIndex(m_familyIndex);

        std::vector<char const *> wantExtensionNames =
            {
                VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME,
                VK_KHR_MAINTENANCE1_EXTENSION_NAME,
                VK_KHR_SAMPLER_YCBCR_CONVERSION_EXTENSION_NAME,
                VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME,
                VK_KHR_IMAGE_FORMAT_LIST_EXTENSION_NAME,
                VK_KHR_BIND_MEMORY_2_EXTENSION_NAME,
                VK_KHR_SWAPCHAIN_EXTENSION_NAME};

        std::vector<vk::ExtensionProperties> extentions;
        std::tie(status, extentions) = m_physical.enumerateDeviceExtensionProperties();
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to enumerateDeviceExtensionProperties." << std::endl;
                return false;
        }

        std::set<std::string> device_extentions;
        std::vector<char const *> deviceExtensionNames;
        for (std::vector<vk::ExtensionProperties>::const_iterator x = extentions.begin(); x != extentions.end(); ++x)
                for (std::vector<char const *>::const_iterator y = wantExtensionNames.begin(); y != wantExtensionNames.end(); ++y)
                        if (0 == strcmp(x->extensionName, *y))
                        {
                                deviceExtensionNames.push_back(*y);
                                device_extentions.insert(*y);
                        }

        vk::DeviceCreateInfo deviceCreateInfo;
        deviceCreateInfo.setQueueCreateInfoCount(1);
        deviceCreateInfo.setPQueueCreateInfos(&deviceQueueCreateInfo);
        deviceCreateInfo.setEnabledExtensionCount(deviceExtensionNames.size());
        deviceCreateInfo.setPpEnabledExtensionNames(deviceExtensionNames.data());

        std::tie(status, m_logical) = m_physical.createDevice(deviceCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create logical device." << std::endl;
                return false;
        }

        return CreateDevice();
}

bool FindPhysicalDevice()
{
        std::vector<vk::PhysicalDevice> physicalDevices;
        std::tie(status, physicalDevices) = instance.enumeratePhysicalDevices();
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to find Vulkan capable devices." << std::endl;
                return false;
        }

        auto const queueFamilyProperties = physicalDevices[0].getQueueFamilyProperties();

        for (uint32_t i = 0; i < queueFamilyProperties.size(); ++i)
        {
                if ((queueFamilyProperties[i].queueFlags & vk::QueueFlagBits::eGraphics) &&
                    glfwGetPhysicalDevicePresentationSupport(instance, physicalDevices[0], i))
                {
                        m_physical = physicalDevices[0];
                        m_familyIndex = i;
                        return CreateLogicalDevice();
                }
        }

        status = vk::Result::eErrorInitializationFailed;
        std::cerr << "Failed to find discrete gpu." << std::endl;
        return false;
}

bool OutSurfaceInit()
{
        return CreateRenderPass() &&
               CreateSwapchainImageViews() &&
               CreateDescriptorSetOne() &&
               CreateUniformBufferOne() &&
               CreatePipeline() &&
               true;
}

bool InsertSurface()
{
        return GetSurfaceSupportKHR() &&
               CreateSurfaceSwapchain() &&
               CreateSurfaceSemaphore() &&
               OutSurfaceInit() &&
               true;
}

bool SetupRender()
{
        return CreateInstance() &&
               CreateSurface() &&
               FindPhysicalDevice() &&
               InsertSurface() &&
               true;
}

bool IsValid() { return !glfwWindowShouldClose(data_window); }

int main()
{
        if (!SetupRender())
                std::exit(EXIT_FAILURE);

        while (IsValid())
        {
                glfwPollEvents();
                Frame();
        }

        std::exit(EXIT_SUCCESS);
        return 17;
}
