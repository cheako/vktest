# Copyright (C) 2018 by Michael Mestnik
# This code is licensed under the MIT license (MIT)
# (http://opensource.org/licenses/MIT)

cmake_minimum_required (VERSION 3.9.6)
project (vulkan-test)
include(FindVulkan)
find_package(ECM REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

# https://blog.kitware.com/cmake-and-the-default-build-type/
# Set a default build type if none was specified
set(default_build_type "Release")
if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
  set(default_build_type "Debug")
endif()

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
      STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release")
endif()

#Vulkan configs
add_definitions(
    -DVULKAN_HPP_NO_EXCEPTIONS
    -DVULKAN_HPP_TYPESAFE_CONVERSION
    -DVULKAN_HPP_NO_SMART_HANDLE
)

#GLM configs
add_definitions(
    -DGLM_ENABLE_EXPERIMENTAL
)

if(NOT ${Vulkan_FOUND})
	message(FATAL_ERROR "Could not find Vulkan library!")
endif()

include_directories(
    ${Vulkan_INCLUDE_DIRS}
    include
    ${CMAKE_BINARY_DIR}
    spirv-tools
)

find_package(glfw3 REQUIRED)

find_library(LIB_GLSLANG glslang)
find_library(LIB_OSDependent OSDependent)
find_library(LIB_PHTREAD pthread)
find_library(LIB_OGLCompiler OGLCompiler)
find_library(LIB_HLSL HLSL)
find_library(LIB_SPIRV SPIRV)
link_libraries(
    ${Vulkan_LIBRARIES}
    ${LIB_GLSLANG}
    ${LIB_OSDependent}
    ${LIB_PHTREAD}
    ${LIB_OGLCompiler}
    ${LIB_HLSL}
    ${LIB_SPIRV}
)

# Feel free to use local copies of shaderc and spirv-tools.
file(GLOB shaderc shaderc/*.cc)
add_library(LIBSHADERC STATIC ${shaderc})
file(GLOB spirv_tools spirv-tools/*.cpp)
add_library(LIBSPIRVTOOLS STATIC ${spirv_tools})
file(GLOB spirv_tools_opt spirv-tools/opt/*.cpp)
add_library(LIBSPIRVTOOLSOPT STATIC ${spirv_tools_opt})
file(GLOB spirv_tools_util spirv-tools/util/*.cpp)
add_library(LIBSPIRVTOOLSUTIL STATIC ${spirv_tools_util})
file(GLOB spirv_tools_val spirv-tools/val/*.cpp)
add_library(LIBSPIRVTOOLSVAL STATIC ${spirv_tools_val})

file(GLOB main src/*.cpp)
add_executable(vulkan-test ${main})
target_link_libraries(vulkan-test
    glfw
    LIBSHADERC
    LIBSPIRVTOOLS
    LIBSPIRVTOOLSOPT
    LIBSPIRVTOOLSUTIL
    LIBSPIRVTOOLSVAL
)

