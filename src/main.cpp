/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "vulkanio.hpp"
#include "render/outsurface.hpp"

namespace vkc
{

class Compositor
{
      public:
        ~Compositor() = default;

        bool Init(int argc, char *argv[]);

        bool IsValid();

        void RenderFrame();

      private:
        VulkanIO m_VulkanIO;
        Render m_Render;
};

bool Compositor::Init(int argc, char *argv[])
{
        bool result;

        singliton_render = &m_Render;

        assert(result = m_Render.Init());
        assert(result = m_VulkanIO.Init());

        return result;
}

inline bool Compositor::IsValid()
{
        return m_VulkanIO.IsValid();
}

inline void Compositor::RenderFrame()
{
        m_VulkanIO.PollDisplays();
        m_Render.Frame();
}

} // namespace vkc

int main(int argc, char *argv[])
{
        vkc::Compositor compositor;
        compositor.Init(argc, argv);

        while (compositor.IsValid())
                compositor.RenderFrame();

        return 0;
}
