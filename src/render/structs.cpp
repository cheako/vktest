/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "helpers.hpp"
#include "structs.hpp"

namespace vkc
{

vk::VertexInputBindingDescription const Render::Device::Vertex::s_inputBindingDescription = {
    0, sizeof(Vertex), vk::VertexInputRate::eVertex};
vk::VertexInputAttributeDescription const Render::Device::Vertex::s_inputAttributeDescription[] = {{
    0, 0, vk::Format::eR32G32B32A32Sfloat, offsetof(Vertex, pos)}, {
    1, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, uv)}};

bool Render::Device::Buffer::Stage(Render::Device &device, void const *data, size_t size, vk::BufferUsageFlagBits usage)
{
        return CreateBuffer(device, size, usage) && AllocateDeviceMemory(device, size, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent) && CopyMemory(device, data, size);
}

bool Render::Device::Buffer::CreateBuffer(Render::Device &device, size_t size, vk::BufferUsageFlagBits usage)
{
        vk::BufferCreateInfo bufferCreateInfo;
        bufferCreateInfo.setQueueFamilyIndexCount(1);
        bufferCreateInfo.setPQueueFamilyIndices(&device.m_familyIndex);
        bufferCreateInfo.setSharingMode(vk::SharingMode::eExclusive);
        bufferCreateInfo.setSize(size);
        bufferCreateInfo.setUsage(usage);

        vk::Result result;
        std::tie(result, buffer) = device.m_logical.createBuffer(bufferCreateInfo);
        if (result != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create buffer." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::Buffer::AllocateDeviceMemory(Render::Device &device, size_t size, vk::MemoryPropertyFlags flags)
{
        bool memoryAvailable = false;
        uint32_t memoryTypeIndex = 0;
        std::tie(memoryAvailable, memoryTypeIndex) = FindMemoryTypeIndex(device.m_physical, flags);

        if (!memoryAvailable)
        {
                std::cerr << "Failed to find memory type." << std::endl;
                return false;
        }

        vk::MemoryAllocateInfo memoryAllocateInfo;
        vk::MemoryRequirements memoryRequirements = device.m_logical.getBufferMemoryRequirements(buffer);
        memoryAllocateInfo.setAllocationSize(memoryRequirements.size);
        memoryAllocateInfo.setMemoryTypeIndex(memoryTypeIndex);
        vk::Result result;
        std::tie(result, memory) = device.m_logical.allocateMemory(memoryAllocateInfo);
        if (result != vk::Result::eSuccess)
        {
                std::cerr << "Failed to allocate memory." << std::endl;
                return false;
        }

        result = device.m_logical.bindBufferMemory(buffer, memory, 0);
        if (result != vk::Result::eSuccess)
        {
                std::cerr << "Failed to bind memory to buffer." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::Buffer::CopyMemory(Render::Device &device, void const *data, size_t size)
{
        vk::Result result;
        void *mappedMemory = nullptr;
        std::tie(result, mappedMemory) = device.m_logical.mapMemory(memory, 0, size);
        if (result != vk::Result::eSuccess)
        {
                std::cerr << "Failed to map memory" << std::endl;
                return false;
        }

        memcpy(mappedMemory, data, size);

        device.m_logical.unmapMemory(memory);

        return true;
}

bool Render::Device::Shader::Init(Render::Device &device, const std::string name, const std::string _code, vk::ShaderStageFlagBits stage)
{
        std::vector<uint32_t> shaderCode;
        if (!CompileShader(name, _code, stage == vk::ShaderStageFlagBits::eVertex ? shaderc_vertex_shader : shaderc_fragment_shader, shaderCode))
        {
                std::cerr << "Failed to compile shader: " << name << std::endl;
                return false;
        }

        vk::ShaderModuleCreateInfo shaderModuleCreateInfo;
        shaderModuleCreateInfo.setCodeSize(static_cast<size_t>(shaderCode.size() * sizeof(int)));
        shaderModuleCreateInfo.setPCode(reinterpret_cast<uint32_t *>(shaderCode.data()));
        std::tie(state, shaderModule) = device.m_logical.createShaderModule(shaderModuleCreateInfo);
        if (state != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create shader module: " << name << std::endl;
                return false;
        }

        shaderStage.setPName("main");
        shaderStage.setStage(stage);
        shaderStage.setModule(shaderModule);

        return true;
}

} // namespace vkc