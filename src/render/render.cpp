/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */

#include "device.hpp"

namespace vkc
{

bool Render::Init() { return true; }

void Render::Shutdown()
{
        m_devices.clear();
}

bool Render::CreateDevice(vk::PhysicalDevice physical, vk::Device logical, uint32_t familyIndex, std::set<std::string> extentions)
{
        m_devices.insert(std::make_pair(logical, std::make_shared<Device>()));
        return m_devices[logical]->Init(physical, logical, familyIndex, extentions);
}

bool Render::InsertSurface(vk::Device logical, vk::SurfaceKHR surface) { return m_devices[logical]->InsertSurface(surface); }
void Render::RemoveSurface(vk::Device logical, vk::SurfaceKHR surface) { m_devices[logical]->RemoveSurface(surface); };

bool Render::Frame()
{
        bool ret = true;
        for (Aa::iterator d = m_devices.begin(); d != m_devices.end(); ++d)
                ret = d->second->Frame() && ret;
        return ret;
}

Render::~Render() { Shutdown(); }

Render *singliton_render;

} // namespace vkc
