/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "outsurface.hpp"
#include "structs.hpp"
#include <iostream>
#include <unistd.h>

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

#define GLM_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace vkc
{

bool Render::Device::OutSurface::Init(Device *_device, vk::SurfaceKHR surface, vk::SwapchainKHR swapchain, vk::Semaphore imageAvailableSemaphore)
{
        device = _device;
        m_surface = surface;
        m_swapchain = swapchain;
        m_imageAvailableSemaphore = imageAvailableSemaphore;
        return CreateRenderPass() &&
               CreateSwapchainImageViews() &&
               CreateDescriptorSet() &&
               CreateUniformBuffer() &&
               CreatePipeline() &&
               true;
}

void Render::Device::OutSurface::Shutdown()
{
        auto m_logical = device->m_logical;

        if (m_descriptorSet)
        {
                status = device->m_logical.freeDescriptorSets(device->m_descriptorPool, m_descriptorSet);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Warning: Failed to free descriptor set." << std::endl;
                }
        }

        if (m_imageAvailableSemaphore)
                m_logical.destroySemaphore(m_imageAvailableSemaphore);
        if (m_renderPass)
                m_logical.destroyRenderPass(m_renderPass);
        if (m_pipeline)
                m_logical.destroyPipeline(m_pipeline);
        for (auto it : m_nextImages)
        {
                if (it.m_framebuffer)
                        m_logical.destroyFramebuffer(it.m_framebuffer);
                if (it.m_imageView)
                        m_logical.destroyImageView(it.m_imageView);
        }
        if (m_uniformBuffer && m_uniformAllocation)
                vmaDestroyBuffer(device->m_allocator, m_uniformBuffer, m_uniformAllocation);
        if (m_swapchain)
                m_logical.destroySwapchainKHR(m_swapchain);
}

bool Render::Device::OutSurface::Frame()
{
        auto m_logical = device->m_logical;
        auto m_commandBuffer = device->m_commandBuffer;
        auto m_vertexBuffer = device->m_vertexBuffer->buffer;
        std::tie(status, m_currentFrameBuffer) = m_logical.acquireNextImageKHR(
            m_swapchain, UINT64_MAX, m_imageAvailableSemaphore, nullptr);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to acquire framebuffer image." << std::endl;
                return false;
        }

        vk::RenderPassBeginInfo renderPassBegin;
        renderPassBegin.setFramebuffer(m_nextImages[m_currentFrameBuffer].m_framebuffer);
        renderPassBegin.setRenderArea(vk::Rect2D({0, 0}, {800, 600}));
        renderPassBegin.setRenderPass(m_renderPass);
        m_commandBuffer.beginRenderPass(renderPassBegin, vk::SubpassContents::eInline);
        m_commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_pipeline);
        {
                vk::DeviceSize offsets[] = {0};
                m_commandBuffer.bindVertexBuffers(0, 1, &m_vertexBuffer, offsets);
        }

        m_commandBuffer.bindDescriptorSets(
            vk::PipelineBindPoint::eGraphics,
            device->m_pipelineLayout, 1,
            m_descriptorSet, nullptr);

        m_commandBuffer.bindDescriptorSets(
            vk::PipelineBindPoint::eGraphics,
            device->m_pipelineLayout, 0,
            device->m_descriptorSet, nullptr);
        m_commandBuffer.draw(6, 1, 0, 0);

        m_commandBuffer.endRenderPass();

        return true;
}

bool Render::Device::OutSurface::CreatePipeline()
{
        auto m_logical = device->m_logical;
        auto m_vertexShader = device->m_vertexShader->shaderStage;
        auto m_fragmentShader = device->m_fragmentShader->shaderStage;
        auto m_pipelineLayout = device->m_pipelineLayout;
        auto m_pipelineCache = device->m_pipelineCache;

        vk::PipelineVertexInputStateCreateInfo vertexInputCreateInfo;
        vertexInputCreateInfo.setVertexAttributeDescriptionCount(2);
        vertexInputCreateInfo.setPVertexAttributeDescriptions(Vertex::s_inputAttributeDescription);
        vertexInputCreateInfo.setVertexBindingDescriptionCount(1);
        vertexInputCreateInfo.setPVertexBindingDescriptions(&Vertex::s_inputBindingDescription);

        vk::PipelineInputAssemblyStateCreateInfo inputAssemblyCreateInfo;
        inputAssemblyCreateInfo.setTopology(vk::PrimitiveTopology::eTriangleList);
        inputAssemblyCreateInfo.setPrimitiveRestartEnable(false);

        vk::PipelineShaderStageCreateInfo const shaderStages[2] = {
            m_vertexShader, m_fragmentShader};

        vk::Viewport viewport;
        viewport.setWidth(800);
        viewport.setHeight(600);
        viewport.setX(0);
        viewport.setY(0);
        viewport.setMaxDepth(1.0f);
        viewport.setMinDepth(0);

        vk::Rect2D scissor;
        scissor.setOffset({0, 0});
        scissor.setExtent({800, 600});

        vk::PipelineViewportStateCreateInfo viewportCreateInfo;
        viewportCreateInfo.setScissorCount(1);
        viewportCreateInfo.setPScissors(&scissor);
        viewportCreateInfo.setViewportCount(1);
        viewportCreateInfo.setPViewports(&viewport);

        vk::PipelineRasterizationStateCreateInfo rasterizationCreateInfo;
        rasterizationCreateInfo.setCullMode(vk::CullModeFlagBits::eNone);
        rasterizationCreateInfo.setPolygonMode(vk::PolygonMode::eFill);
        rasterizationCreateInfo.setFrontFace(vk::FrontFace::eClockwise);
        rasterizationCreateInfo.setLineWidth(1.0f);
        rasterizationCreateInfo.setDepthClampEnable(false);
        rasterizationCreateInfo.setDepthBiasEnable(false);
        rasterizationCreateInfo.setRasterizerDiscardEnable(false);

        vk::PipelineMultisampleStateCreateInfo multisamplingCreateInfo;
        multisamplingCreateInfo.setSampleShadingEnable(false);
        multisamplingCreateInfo.setRasterizationSamples(vk::SampleCountFlagBits::e1);

        vk::PipelineColorBlendAttachmentState colorBlendAttachmentState;
        colorBlendAttachmentState.setBlendEnable(false);
        colorBlendAttachmentState.setColorWriteMask(
            vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA);

        vk::PipelineColorBlendStateCreateInfo colorBlendCreateInfo;
        colorBlendCreateInfo.setAttachmentCount(1);
        colorBlendCreateInfo.setPAttachments(&colorBlendAttachmentState);
        colorBlendCreateInfo.setLogicOpEnable(false);

        vk::GraphicsPipelineCreateInfo pipelineCreateInfo;
        pipelineCreateInfo.setPVertexInputState(&vertexInputCreateInfo);
        pipelineCreateInfo.setPInputAssemblyState(&inputAssemblyCreateInfo);
        pipelineCreateInfo.setStageCount(2);
        pipelineCreateInfo.setPStages(shaderStages);
        pipelineCreateInfo.setPViewportState(&viewportCreateInfo);
        pipelineCreateInfo.setPRasterizationState(&rasterizationCreateInfo);
        pipelineCreateInfo.setPMultisampleState(&multisamplingCreateInfo);
        pipelineCreateInfo.setPColorBlendState(&colorBlendCreateInfo);
        pipelineCreateInfo.setRenderPass(m_renderPass);
        pipelineCreateInfo.setSubpass(0);
        pipelineCreateInfo.setLayout(m_pipelineLayout);

        std::tie(status, m_pipeline) = m_logical.createGraphicsPipeline(m_pipelineCache, pipelineCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create graphics pipeline." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::OutSurface::CreateDescriptorSet()
{
        vk::DescriptorSetAllocateInfo allocInfo;
        allocInfo.setDescriptorPool(device->m_descriptorPool);
        allocInfo.setDescriptorSetCount(1);
        allocInfo.setPSetLayouts(&device->m_descriptorLayouts[1]);

        std::vector<vk::DescriptorSet> sets;
        std::tie(status, sets) = device->m_logical.allocateDescriptorSets(allocInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create render done semaphore." << std::endl;
                return false;
        }

        m_descriptorSet = sets[0];

        return true;
}

bool Render::Device::OutSurface::CreateUniformBuffer()
{
        vk::BufferCreateInfo bufferInfo;
        bufferInfo.setSize(sizeof(glm::mat4));
        bufferInfo.setUsage(vk::BufferUsageFlagBits::eUniformBuffer);

        VkBufferCreateInfo tmpInfo;
        tmpInfo = bufferInfo;

        VmaAllocationCreateInfo allocCreateInfo = {};
        allocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        allocCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        VkBuffer tmpBuffer;
        status = vk::Result(vmaCreateBuffer(device->m_allocator, &tmpInfo, &allocCreateInfo, &tmpBuffer, &m_uniformAllocation, nullptr));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create uniform buffer." << std::endl;
                return false;
        }

        m_uniformBuffer = tmpBuffer;

#include "uniform_outsurface.h"
        void *uniform;
        vmaMapMemory(device->m_allocator, m_uniformAllocation, reinterpret_cast<void **>(&uniform));
        memcpy(uniform, uniform_outsurface_8a2lEQ, uniform_outsurface_8a2lEQ_len);
        vmaUnmapMemory(device->m_allocator, m_uniformAllocation);

#if 0
        char filename[] = "uniform_outsurface.XXXXXX";
        int fd = mkstemp(filename);
        if (fd != -1)
        {
                glm::mat4 tmp = projMat * viewMat;
                write(fd, &tmp, sizeof(glm::mat4));
                close(fd);
        }
#endif

        vk::DescriptorBufferInfo descriptorInfo;
        descriptorInfo.setBuffer(m_uniformBuffer);
        descriptorInfo.setRange(sizeof(glm::mat4));

        vk::WriteDescriptorSet writeInfo;
        writeInfo.setDescriptorType(vk::DescriptorType::eUniformBuffer);
        writeInfo.setDescriptorCount(1);
        writeInfo.setPBufferInfo(&descriptorInfo);
        writeInfo.setDstSet(m_descriptorSet);
        device->m_logical.updateDescriptorSets(writeInfo, nullptr);

        return true;
}

bool Render::Device::OutSurface::CreateRenderPass()
{
        auto m_logical = device->m_logical;
        vk::AttachmentDescription attachmentDescription;
        attachmentDescription.setFormat(vk::Format::eB8G8R8A8Srgb);
        attachmentDescription.setSamples(vk::SampleCountFlagBits::e1);
        attachmentDescription.setInitialLayout(vk::ImageLayout::eUndefined);
        attachmentDescription.setFinalLayout(vk::ImageLayout::ePresentSrcKHR);
        attachmentDescription.setLoadOp(vk::AttachmentLoadOp::eLoad);
        attachmentDescription.setStoreOp(vk::AttachmentStoreOp::eStore);
        attachmentDescription.setStencilLoadOp(vk::AttachmentLoadOp::eDontCare);
        attachmentDescription.setStencilStoreOp(vk::AttachmentStoreOp::eDontCare);

        vk::AttachmentReference attachmentReference;
        attachmentReference.setAttachment(0);
        attachmentReference.setLayout(vk::ImageLayout::eColorAttachmentOptimal);

        vk::SubpassDescription subpass;
        subpass.setColorAttachmentCount(1);
        subpass.setPColorAttachments(&attachmentReference);
        subpass.setPipelineBindPoint(vk::PipelineBindPoint::eGraphics);

        vk::RenderPassCreateInfo renderPassCreateInfo;
        renderPassCreateInfo.setAttachmentCount(1);
        renderPassCreateInfo.setPAttachments(&attachmentDescription);
        renderPassCreateInfo.setSubpassCount(1);
        renderPassCreateInfo.setPSubpasses(&subpass);

        std::tie(status, m_renderPass) = m_logical.createRenderPass(renderPassCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create renderpass." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::OutSurface::CreateSwapchainImageViews()
{
        auto m_logical = device->m_logical;
        std::vector<vk::Image> images;
        std::tie(status, images) = m_logical.getSwapchainImagesKHR(m_swapchain);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get swapchain images." << std::endl;
                return false;
        }

        m_nextImages.resize(images.size());
        for (size_t i = 0; i < images.size(); ++i)
        {
                m_nextImages[i].m_image = images[i];

                vk::ImageSubresourceRange range;
                range.setLayerCount(1);
                range.setLevelCount(1);
                range.setAspectMask(vk::ImageAspectFlagBits::eColor);

                vk::ImageViewCreateInfo imageViewCreateInfo;
                imageViewCreateInfo.setFormat(vk::Format::eB8G8R8A8Srgb);
                imageViewCreateInfo.setImage(images[i]);
                imageViewCreateInfo.setViewType(vk::ImageViewType::e2D);
                imageViewCreateInfo.setSubresourceRange(range);
                imageViewCreateInfo.setComponents({vk::ComponentSwizzle::eR, vk::ComponentSwizzle::eG, vk::ComponentSwizzle::eB, vk::ComponentSwizzle::eA});

                std::tie(status, m_nextImages[i].m_imageView) = m_logical.createImageView(imageViewCreateInfo);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create swapchain image view." << std::endl;
                        return false;
                }

                vk::FramebufferCreateInfo framebufferCreateInfo;
                framebufferCreateInfo.setRenderPass(m_renderPass);
                framebufferCreateInfo.setAttachmentCount(1);
                framebufferCreateInfo.setPAttachments(&m_nextImages[i].m_imageView);
                framebufferCreateInfo.setWidth(800);
                framebufferCreateInfo.setHeight(600);
                framebufferCreateInfo.setLayers(1);

                std::tie(status, m_nextImages[i].m_framebuffer) = m_logical.createFramebuffer(framebufferCreateInfo);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create framebuffer." << std::endl;
                        return false;
                }
        }

        return true;
}

} // namespace vkc