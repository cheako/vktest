/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include "helpers.hpp"
#include "vulkan.hpp"
#include "vk_mem_alloc.h"

#include <cstdint>
#include <utility>
#include <map>
#include <vector>
#include <set>

namespace vkc
{

constexpr struct VertexData
{
        struct VertexPos
        {
                float x;
                float y;
                float z;
                float w;
        } pos;
        struct VertexUV
        {
                float x;
                float y;
        } uv;
} s_vertices[6] = {
#if 0
    {{-1,
      -1,
      1,
      1},
     {0, 1}},
    {{-1,
      1,
      1,
      1},
     {0, 0}},
    {{1,
      1,
      1,
      1},
     {1, 0}},
    {{1,
      -1,
      1,
      1},
     {1, 1}},
    {{-1,
      -1,
      1,
      1},
     {0, 1}},
    {{1,
      1,
      1,
      1},
     {1, 0}}
#else
    {{1,
      1,
      1,
      1},
     {1, 0}},
    {{-1,
      1,
      1,
      1},
     {0, 0}},
    {{-1,
      -1,
      1,
      1},
     {0, 1}},
    {{1,
      1,
      1,
      1},
     {1, 0}},
    {{-1,
      -1,
      1,
      1},
     {0, 1}},
    {{1,
      -1,
      1,
      1},
     {1, 1}}
#endif
};

struct region_addition
{
        bool negative;
        int32_t x;
        int32_t y;
        int32_t width;
        int32_t height;
};
typedef std::vector<region_addition> region_changelist;

struct damage
{
        int32_t x;
        int32_t y;
        int32_t width;
        int32_t height;
};
typedef std::vector<damage> damage_list;

class Wayland_Compositor_Surface;

class Render
{
      public:
        ~Render();
        
        bool Init();

        void Shutdown();

        bool Frame();

        vk::Result status = vk::Result::eErrorInitializationFailed;

        bool CreateDevice(vk::PhysicalDevice physical, vk::Device logical, uint32_t familyIndex, std::set<std::string> extentions);
        bool InsertSurface(vk::Device logical, vk::SurfaceKHR surface);
        void RemoveSurface(vk::Device logical, vk::SurfaceKHR surface);

      private:
        class Device;
        typedef std::map<vk::Device, std::shared_ptr<Device>> Aa;
        Aa m_devices;
};

extern Render *singliton_render;

} // namespace vkc