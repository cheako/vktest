/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "outsurface.hpp"
#include "structs.hpp"
#include <iostream>
#include <unistd.h>

#include "vk_mem_alloc.h"

#define GLM_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace vkc
{

bool Render::Device::Init(vk::PhysicalDevice p, vk::Device d, uint32_t familyIndex, std::set<std::string> device_extentions)
{
        m_logical = d;
        m_physical = p;
        m_familyIndex = familyIndex;
        m_queue = m_logical.getQueue(familyIndex, 0);
        m_device_extentions = device_extentions;
        return CreateSemaphore() &&
               CreateFence() &&
               CreateSampler() &&
               CreateShaders() &&
               CreateVMA() &&
               CreateDescriptorPool() &&
               CreateDescriptorLayout() &&
               CreatePipeline() &&
               CreateVertexBuffer() &&
               CreateCommandPool() &&
               CreatePrimaryBuffer() &&
               CreateDescriptorSet() &&
               CreateUniformBuffer() &&
               true;
}

void Render::Device::Shutdown()
{
        // m_logical.waitIdle();
        for (auto it = m_surfaces.cbegin(); it != m_surfaces.cend();)
        {
                delete it->second;
                m_surfaces.erase(it++);
        }

        if (m_descriptorSet)
        {
                status = m_logical.freeDescriptorSets(m_descriptorPool, m_descriptorSet);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Warning: Failed to free descriptor set." << std::endl;
                }
        }

        if (m_uniformBuffer && m_uniformAllocation)
                vmaDestroyBuffer(m_allocator, m_uniformBuffer, m_uniformAllocation);
        if (m_buffer && m_allocation)
                vmaDestroyBuffer(m_allocator, m_buffer, m_allocation);
        if (m_textureImageView)
                m_logical.destroyImageView(m_textureImageView);
        if (m_textureImage && m_textureAllocation)
                vmaDestroyImage(m_allocator, m_textureImage, m_textureAllocation);
        if (m_commandBuffer)
                m_logical.freeCommandBuffers(m_commandPool, m_commandBuffer);
        if (m_commandPool)
                m_logical.destroyCommandPool(m_commandPool);
        if (m_vertexBuffer->buffer)
                m_logical.destroyBuffer(m_vertexBuffer->buffer);
        if (m_vertexBuffer->memory)
                m_logical.freeMemory(m_vertexBuffer->memory);
        if (m_vertexShader)
                m_logical.destroyShaderModule(m_vertexShader->shaderModule);
        if (m_sampler)
                m_logical.destroySampler(m_sampler);
        if (m_fragmentShader)
                m_logical.destroyShaderModule(m_fragmentShader->shaderModule);
        if (m_pipelineCache)
                m_logical.destroyPipelineCache(m_pipelineCache);
        if (m_pipelineLayout)
                m_logical.destroyPipelineLayout(m_pipelineLayout);
        if (m_descriptorLayouts[1])
                m_logical.destroyDescriptorSetLayout(m_descriptorLayouts[1]);
        if (m_descriptorLayouts[0])
                m_logical.destroyDescriptorSetLayout(m_descriptorLayouts[0]);
        if (m_descriptorPool)
                m_logical.destroyDescriptorPool(m_descriptorPool);
        if (m_allocator)
                vmaDestroyAllocator(m_allocator);
        if (m_fence)
                m_logical.destroyFence(m_fence);
        if (m_renderDoneSemaphore)
                m_logical.destroySemaphore(m_renderDoneSemaphore);
}

bool Render::Device::InsertSurface(vk::SurfaceKHR surface)
{
        if (GetSurfaceSupportKHR(surface) && CreateSurfaceSwapchain(surface) && CreateSurfaceSemaphore())
        {
                m_surfaces.insert(std::make_pair(surface, new OutSurface()));
                if (m_surfaces[surface]->Init(
                        this,
                        surface,
                        m_swapchains.back(),
                        m_imageAvailableSemaphores.back()))
                        return true;
                delete m_surfaces[surface];
                m_surfaces.erase(surface);
        }
        return false;
}

void Render::Device::RemoveSurface(vk::SurfaceKHR surface)
{
        delete m_surfaces[surface];
        m_imageAvailableSemaphores.erase(std::remove(m_imageAvailableSemaphores.begin(), m_imageAvailableSemaphores.end(), m_surfaces[surface]->m_imageAvailableSemaphore), m_imageAvailableSemaphores.end());
        m_swapchains.erase(std::remove(m_swapchains.begin(), m_swapchains.end(), m_surfaces[surface]->m_swapchain), m_swapchains.end());
        m_surfaces.erase(surface);
}

const static vk::ImageSubresourceRange subresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

bool Render::Device::Frame()
{
        status = m_logical.waitIdle();
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to go idle." << std::endl;
                return false;
        }

        vk::CommandBufferBeginInfo beginInfo;
        beginInfo.setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
        m_commandBuffer.begin(beginInfo);

        {
#include "image_info.h"
#include "image_data.h"
                static bool init = true;
                if (init)
                {
                        init = false;

                        m_offset = reinterpret_cast<uintptr_t>(image_data_0DyGJS) % 32;
                        vk::BufferCreateInfo bufferInfo;
                        bufferInfo.setSize(stride * height + m_offset);
                        bufferInfo.setUsage(vk::BufferUsageFlagBits::eTransferSrc);
                        bufferInfo.setSharingMode(vk::SharingMode::eExclusive);

                        VkBufferCreateInfo tmpInfo;
                        tmpInfo = bufferInfo;

                        VmaAllocationCreateInfo bufferAllocInfo = {};
                        bufferAllocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

                        VkBuffer tmpBuffer;
                        status = vk::Result(vmaCreateBuffer(m_allocator, &tmpInfo, &bufferAllocInfo, &tmpBuffer, &m_allocation, nullptr));
                        if (status != vk::Result::eSuccess)
                        {
                                std::cerr << "Failed to create uniform buffer." << std::endl;
                                return false;
                        }

                        m_buffer = tmpBuffer;

                        vk::ImageCreateInfo imageInfo;
                        imageInfo.setImageType(vk::ImageType::e2D);
                        imageInfo.setExtent(vk::Extent3D(width, height, 1));
                        imageInfo.setMipLevels(1);
                        imageInfo.setArrayLayers(1);

                        imageInfo.setFormat(vk::Format(format));
                        imageInfo.setTiling(vk::ImageTiling::eLinear);
                        imageInfo.setInitialLayout(vk::ImageLayout::eUndefined);
                        imageInfo.setUsage(vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled);
                        imageInfo.setSamples(vk::SampleCountFlagBits::e1);

                        VkImageCreateInfo tmpImageInfo;
                        tmpImageInfo = imageInfo;

                        VmaAllocationCreateInfo allocInfo = {};
                        allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                        VkImage imageTemp;
                        status = vk::Result(vmaCreateImage(
                            m_allocator,
                            &tmpImageInfo, &allocInfo, &imageTemp,
                            &m_textureAllocation, nullptr));
                        if (status != vk::Result::eSuccess)
                        {
                                std::cerr << "failed to create image!" << std::endl;
                                return false;
                        }

                        m_textureImage = imageTemp;

                        vk::ImageViewCreateInfo viewInfo;
                        viewInfo.setImage(m_textureImage);
                        viewInfo.setViewType(vk::ImageViewType::e2D);
                        viewInfo.setFormat(vk::Format(format));
                        viewInfo.setSubresourceRange(subresourceRange);

                        std::tie(status, m_textureImageView) = m_logical.createImageView(viewInfo);
                        if (status != vk::Result::eSuccess)
                        {
                                std::cerr << "failed to allocate texture view!" << std::endl;
                                return false;
                        }

                        vk::DescriptorImageInfo descriptorImageInfo;
                        descriptorImageInfo.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
                        descriptorImageInfo.setImageView(m_textureImageView);
                        descriptorImageInfo.setSampler(m_sampler);

                        vk::WriteDescriptorSet writeInfo;
                        writeInfo.setDescriptorCount(1);
                        writeInfo.setDstBinding(1);
                        writeInfo.setDescriptorType(vk::DescriptorType::eCombinedImageSampler);
                        writeInfo.setPImageInfo(&descriptorImageInfo);
                        writeInfo.setDstSet(m_descriptorSet);
                        m_logical.updateDescriptorSets(writeInfo, nullptr);

                        char *buffer_data;
                        status = vk::Result(vmaMapMemory(m_allocator, m_allocation, reinterpret_cast<void **>(&buffer_data)));
                        if (status != vk::Result::eSuccess)
                        {
                                std::cerr << "failed to map image memory!" << std::endl;
                                return false;
                        }

#if 1
                        memcpy(buffer_data + m_offset, image_data_0DyGJS, image_data_0DyGJS_len);
#else
                        memset(buffer_data + m_offset, 0x88, image_data_0DyGJS_len);
#endif
                        vmaUnmapMemory(m_allocator, m_allocation);

                        vmaFlushAllocation(m_allocator, m_allocation, 0, stride * height);

#define XFER_EVERY_TIME 1
#if XFER_EVERY_TIME
                }
#endif

                vk::ImageMemoryBarrier barrier;
                barrier.setOldLayout(vk::ImageLayout::eUndefined);
                barrier.setNewLayout(vk::ImageLayout::eTransferDstOptimal);
                barrier.setDstAccessMask(vk::AccessFlagBits::eTransferWrite);
                barrier.setSubresourceRange(subresourceRange);

                barrier.setImage(m_textureImage);

                m_commandBuffer.pipelineBarrier(
                    vk::PipelineStageFlagBits::eTopOfPipe,
                    vk::PipelineStageFlagBits::eTransfer,
                    (vk::DependencyFlags)0,
                    nullptr, nullptr, barrier);

                const vk::ImageSubresourceLayers subresourceLayers(vk::ImageAspectFlagBits::eColor, 0, 0, 1);

                vk::BufferImageCopy region;
                region.setImageOffset(m_offset);
                region.setBufferRowLength(stride / 4);
                region.setImageSubresource(subresourceLayers);

                region.setImageOffset(vk::Offset3D(0, 0, 0));
                region.setImageExtent(vk::Extent3D(
                    width,
                    height,
                    1));

                m_commandBuffer.copyBufferToImage(
                    m_buffer,
                    m_textureImage,
                    vk::ImageLayout::eTransferDstOptimal,
                    region);

                barrier.setOldLayout(vk::ImageLayout::eTransferDstOptimal);
                barrier.setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
                barrier.setSrcAccessMask(vk::AccessFlagBits::eTransferWrite);
                barrier.setDstAccessMask(vk::AccessFlagBits::eShaderRead);

                m_commandBuffer.pipelineBarrier(
                    vk::PipelineStageFlagBits::eTransfer,
                    vk::PipelineStageFlagBits::eFragmentShader,
                    (vk::DependencyFlags)0,
                    nullptr, nullptr,
                    barrier);
#if !XFER_EVERY_TIME
        }
#endif
}
bool ret = true;
for (Ac::iterator b = m_surfaces.begin(); b != m_surfaces.end(); ++b)
        ret = b->second->Frame() && ret;

m_commandBuffer.end();

vk::SubmitInfo submitInfo;
submitInfo.setCommandBufferCount(1);
submitInfo.setPCommandBuffers(&m_commandBuffer);
vk::PipelineStageFlags waitStage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
submitInfo.setPWaitDstStageMask(&waitStage);
submitInfo.setWaitSemaphoreCount(static_cast<uint32_t>(m_imageAvailableSemaphores.size()));
submitInfo.setPWaitSemaphores(m_imageAvailableSemaphores.data());
submitInfo.setSignalSemaphoreCount(1);
submitInfo.setPSignalSemaphores(&m_renderDoneSemaphore);

status = m_queue.submit(1, &submitInfo, m_fence);
if (status != vk::Result::eSuccess)
{
        std::cerr << "Failed to submit cmd." << std::endl;
        return false;
}

std::vector<vk::SwapchainKHR> swapchains;
std::vector<uint32_t> framebuffers;

for (Ac::iterator b = m_surfaces.begin(); b != m_surfaces.end(); ++b)
{
        swapchains.insert(swapchains.end(), b->second->m_swapchain);
        framebuffers.insert(framebuffers.end(), b->second->m_currentFrameBuffer);
}

while (m_logical.waitForFences(vk::ArrayProxy<const vk::Fence>(m_fence), true, UINT64_MAX) == vk::Result::eTimeout)
        ;
m_logical.resetFences(vk::ArrayProxy<const vk::Fence>(m_fence));

vk::PresentInfoKHR presentInfo;
presentInfo.setWaitSemaphoreCount(1);
presentInfo.setPWaitSemaphores(&m_renderDoneSemaphore);
presentInfo.setSwapchainCount(swapchains.size());
presentInfo.setPSwapchains(swapchains.data());
presentInfo.setPImageIndices(framebuffers.data());
status = m_queue.presentKHR(presentInfo);
if (status != vk::Result::eSuccess)
{
        std::cerr << "Failed to present queue." << std::endl;
        return false;
}

return ret;
} // namespace vkc

bool Render::Device::GetSurfaceSupportKHR(vk::SurfaceKHR surface)
{
        vk::Bool32 supported;
        std::tie(status, supported) = m_physical.getSurfaceSupportKHR(m_familyIndex, surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface support." << std::endl;
                return false;
        }
        return supported;
}

bool Render::Device::CreateSemaphore()
{
        vk::SemaphoreCreateInfo createInfo;
        std::tie(status, m_renderDoneSemaphore) = m_logical.createSemaphore(createInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create render done semaphore." << std::endl;
                return false;
        }
        return true;
}

bool Render::Device::CreateFence()
{
        vk::FenceCreateInfo createInfo;
        std::tie(status, m_fence) = m_logical.createFence(createInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create fence." << std::endl;
                return false;
        }
        return true;
}

bool Render::Device::CreateSurfaceSwapchain(vk::SurfaceKHR surface)
{
        vk::SurfaceCapabilitiesKHR capabilities;
        std::vector<vk::SurfaceFormatKHR> imageFormats;
        std::vector<vk::PresentModeKHR> presentModes;

        std::tie(status, capabilities) = m_physical.getSurfaceCapabilitiesKHR(surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface capabilities." << std::endl;
                return false;
        }

        std::tie(status, imageFormats) = m_physical.getSurfaceFormatsKHR(surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface image formats." << std::endl;
                return false;
        }

        std::tie(status, presentModes) = m_physical.getSurfacePresentModesKHR(surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface present modes." << std::endl;
                return false;
        }

        if (!(capabilities.supportedCompositeAlpha & vk::CompositeAlphaFlagBitsKHR::eOpaque))
        {
                std::cerr << "Surface does not support opaque composite alpha." << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        if (capabilities.maxImageCount != 0 && capabilities.maxImageCount < 2)
        {
                std::cerr << "Surface does not support double buffering." << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }
        if (std::find(imageFormats.begin(), imageFormats.end(), (vk::SurfaceFormatKHR){vk::Format::eB8G8R8A8Srgb, vk::ColorSpaceKHR::eSrgbNonlinear}) == imageFormats.end())
        {
                std::cerr << "Device does not support image format." << std::endl;
                for (size_t i = 0; i < imageFormats.size(); ++i)
                        std::cerr << "Format(" << i << "): " << static_cast<uint32_t>(imageFormats[i].format) << ", " << static_cast<uint32_t>(imageFormats[i].colorSpace) << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        vk::PresentModeKHR presentMode = vk::PresentModeKHR::eFifo;
        if (std::find(presentModes.begin(), presentModes.end(), vk::PresentModeKHR::eFifo) == presentModes.end())
        {
                std::cerr << "Device does not support fifo presetn mode." << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        vk::SwapchainCreateInfoKHR swapchainCreateInfo;
        swapchainCreateInfo.setSurface(surface);
        swapchainCreateInfo.setMinImageCount(2);
        swapchainCreateInfo.setImageFormat(vk::Format::eB8G8R8A8Srgb);
        swapchainCreateInfo.setImageColorSpace(vk::ColorSpaceKHR::eSrgbNonlinear);
        swapchainCreateInfo.setImageExtent(vk::Extent2D(800, 600));
        swapchainCreateInfo.setImageArrayLayers(1);
        swapchainCreateInfo.setImageUsage(vk::ImageUsageFlagBits::eColorAttachment);
        swapchainCreateInfo.setImageSharingMode(vk::SharingMode::eExclusive);
        swapchainCreateInfo.setQueueFamilyIndexCount(1);
        swapchainCreateInfo.setPQueueFamilyIndices(&m_familyIndex);
        swapchainCreateInfo.setPreTransform(vk::SurfaceTransformFlagBitsKHR::eIdentity);
        swapchainCreateInfo.setCompositeAlpha(vk::CompositeAlphaFlagBitsKHR::eOpaque);
        swapchainCreateInfo.setPresentMode(presentMode);
        swapchainCreateInfo.setClipped(true);

        vk::SwapchainKHR swapchain;
        std::tie(status, swapchain) = m_logical.createSwapchainKHR(swapchainCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create swapchain" << std::endl;
                return false;
        }

        m_swapchains.insert(m_swapchains.end(), swapchain);

        return true;
}

bool Render::Device::CreateSurfaceSemaphore()
{
        vk::SemaphoreCreateInfo createInfo;
        vk::Semaphore sem;
        std::tie(status, sem) = m_logical.createSemaphore(createInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create framebuffer." << std::endl;
                return false;
        }
        m_imageAvailableSemaphores.insert(m_imageAvailableSemaphores.end(), sem);
        return true;
}

bool Render::Device::CreateSampler()
{
        vk::SamplerCreateInfo samplerInfo;
#if 1
        samplerInfo.setMagFilter(vk::Filter::eNearest);
        samplerInfo.setMinFilter(vk::Filter::eNearest);
        samplerInfo.setAddressModeU(vk::SamplerAddressMode::eClampToEdge);
        samplerInfo.setAddressModeV(vk::SamplerAddressMode::eClampToEdge);
        samplerInfo.setAddressModeW(vk::SamplerAddressMode::eClampToEdge);
        samplerInfo.setBorderColor(vk::BorderColor::eIntOpaqueBlack);
        samplerInfo.setCompareOp(vk::CompareOp::eAlways);
        samplerInfo.setMipmapMode(vk::SamplerMipmapMode::eNearest);
#else
                samplerInfo.setMagFilter(vk::Filter::eLinear);
                samplerInfo.setMinFilter(vk::Filter::eLinear);
                samplerInfo.setAddressModeU(vk::SamplerAddressMode::eRepeat);
                samplerInfo.setAddressModeV(vk::SamplerAddressMode::eRepeat);
                samplerInfo.setAddressModeW(vk::SamplerAddressMode::eRepeat);
                samplerInfo.setAnisotropyEnable(VK_FALSE);
                samplerInfo.setMaxAnisotropy(1);
                samplerInfo.setUnnormalizedCoordinates(VK_FALSE);
                samplerInfo.setCompareEnable(VK_FALSE);
                samplerInfo.setMipLodBias(0.0f);
                samplerInfo.setMinLod(0.0f);
                samplerInfo.setMaxLod(0.0f);
                samplerInfo.setBorderColor(vk::BorderColor::eIntOpaqueWhite);
                samplerInfo.setCompareOp(vk::CompareOp::eAlways);
                samplerInfo.setMipmapMode(vk::SamplerMipmapMode::eLinear);
#endif

        std::tie(status, m_sampler) = m_logical.createSampler(samplerInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create framebuffer." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreateShaders()
{
        m_vertexShader = new Shader();
        m_fragmentShader = new Shader();
        return m_vertexShader->Init(*this, "main_vertex",
                                    R"(#version 450

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec2 inUV;

layout(set = 1, binding = 0) uniform SUBO {
  mat4 projView;
} display;

layout(set = 0, binding = 0) uniform UBO {
  mat4 model;
} surface;

layout(location = 0) out vec2 outUV;

void main()
{
    outUV = inUV;
    /* Trash */
    gl_Position = display.projView * surface.model * inPosition;
    /* Full screen */
    //gl_Position = inPosition;
}
)",
                                    vk::ShaderStageFlagBits::eVertex) &&
               m_fragmentShader->Init(*this, "main_fragment",
                                      R"(#version 450

layout(location = 0) in vec2 inUV;

layout(set = 0, binding = 1) uniform sampler2D surface;

layout(location = 0) out vec4 outColor;

void main()
{
    /* Works, solid color */
    //outColor = vec4(1.0, 0, 0.7, 1.0);
    /* Black */
    outColor = texture(surface, inUV);
    /* Still Black */
    //outColor.rgb = texture(surface, inUV).rgb;
    //outColor.a = 1.0;
    /* Should work */
    //outColor = vec4(inUV, 0.2, 1.0);
}
)",
                                      vk::ShaderStageFlagBits::eFragment);
}

bool Render::Device::CreateVMA()
{
        VmaAllocatorCreateInfo allocatorInfo = {};
        allocatorInfo.physicalDevice = m_physical;
        allocatorInfo.device = m_logical;
        status = vk::Result(vmaCreateAllocator(&allocatorInfo, &m_allocator));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create allocator." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreateDescriptorPool()
{
        std::vector<vk::DescriptorPoolSize> poolSizes = {
            {vk::DescriptorType::eSampler, 4096},
            {vk::DescriptorType::eCombinedImageSampler, 4096},
            {vk::DescriptorType::eSampledImage, 4096},
            {vk::DescriptorType::eStorageImage, 4096},
            {vk::DescriptorType::eUniformTexelBuffer, 4096},
            {vk::DescriptorType::eStorageTexelBuffer, 4096},
            {vk::DescriptorType::eUniformBuffer, 4096},
            {vk::DescriptorType::eStorageBuffer, 4096},
            {vk::DescriptorType::eUniformBufferDynamic, 4096},
            {vk::DescriptorType::eStorageBufferDynamic, 4096},
            {vk::DescriptorType::eInputAttachment, 4096}};

        vk::DescriptorPoolCreateInfo poolInfo;
        poolInfo.setPoolSizeCount(poolSizes.size());
        poolInfo.setPPoolSizes(poolSizes.data());
        poolInfo.setMaxSets(12 * 4096);
        poolInfo.setFlags(vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet);

        std::tie(status, m_descriptorPool) = m_logical.createDescriptorPool(poolInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create descriptor pool." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreateDescriptorLayout()
{
        std::vector<vk::DescriptorSetLayoutBinding> layoutBindings;
        layoutBindings.insert(layoutBindings.end(),
                              vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex));
        layoutBindings.insert(layoutBindings.end(),
                              vk::DescriptorSetLayoutBinding(1, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment));

        vk::DescriptorSetLayoutCreateInfo layoutInfo;
        layoutInfo.setBindingCount(layoutBindings.size());
        layoutInfo.setPBindings(layoutBindings.data());

        std::tie(status, m_descriptorLayouts[0]) = m_logical.createDescriptorSetLayout(layoutInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create descriptor layout." << std::endl;
                return false;
        }

        layoutBindings = {};
        layoutBindings.insert(layoutBindings.end(),
                              vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex));

        layoutInfo.setBindingCount(layoutBindings.size());
        layoutInfo.setPBindings(layoutBindings.data());

        std::tie(status, m_descriptorLayouts[1]) = m_logical.createDescriptorSetLayout(layoutInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create descriptor layout." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreatePipeline()
{
        vk::PipelineLayoutCreateInfo layoutCreateInfo;
        layoutCreateInfo.setSetLayoutCount(2);
        layoutCreateInfo.setPSetLayouts(m_descriptorLayouts);

        std::tie(status, m_pipelineLayout) = m_logical.createPipelineLayout(layoutCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create pipeline layout." << std::endl;
                return false;
        }

        vk::PipelineCacheCreateInfo cacheCreateInfo;
        std::tie(status, m_pipelineCache) = m_logical.createPipelineCache(cacheCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create pipeline cache." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreateVertexBuffer()
{
        m_vertexBuffer = new Buffer();
        return m_vertexBuffer->Stage(*this, s_vertices, sizeof(s_vertices), vk::BufferUsageFlagBits::eVertexBuffer);
}

bool Render::Device::CreateCommandPool()
{
        vk::CommandPoolCreateInfo cmdPoolCreateInfo;
        cmdPoolCreateInfo.setFlags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer);
        cmdPoolCreateInfo.setQueueFamilyIndex(m_familyIndex);

        std::tie(status, m_commandPool) = m_logical.createCommandPool(cmdPoolCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create command pool." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreatePrimaryBuffer()
{
        vk::CommandBufferAllocateInfo cmdAllocInfo;
        cmdAllocInfo.setCommandBufferCount(1);
        cmdAllocInfo.setCommandPool(m_commandPool);
        cmdAllocInfo.setLevel(vk::CommandBufferLevel::ePrimary);

        std::vector<vk::CommandBuffer> storage;
        std::tie(status, storage) = m_logical.allocateCommandBuffers(cmdAllocInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create primary command buffer." << std::endl;
                return false;
        }
        m_commandBuffer = storage.front();

        return true;
}

bool Render::Device::CreateDescriptorSet()
{
        vk::DescriptorSetAllocateInfo allocInfo;
        allocInfo.setDescriptorPool(m_descriptorPool);
        allocInfo.setDescriptorSetCount(1);
        allocInfo.setPSetLayouts(m_descriptorLayouts);

        std::vector<vk::DescriptorSet> sets;
        std::tie(status, sets) = m_logical.allocateDescriptorSets(allocInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create render done semaphore." << std::endl;
                return false;
        }

        m_descriptorSet = sets[0];

        return true;
}

bool Render::Device::CreateUniformBuffer()
{
        vk::BufferCreateInfo bufferInfo;
        bufferInfo.setSize(sizeof(glm::mat4));
        bufferInfo.setUsage(vk::BufferUsageFlagBits::eUniformBuffer);

        VkBufferCreateInfo tmpInfo;
        tmpInfo = bufferInfo;

        VmaAllocationCreateInfo bufferAllocInfo = {};
        bufferAllocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        bufferAllocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        VkBuffer tmpBuffer;
        status = vk::Result(vmaCreateBuffer(m_allocator, &tmpInfo, &bufferAllocInfo, &tmpBuffer, &m_uniformAllocation, nullptr));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create uniform buffer." << std::endl;
                return false;
        }

        m_uniformBuffer = tmpBuffer;

#include "uniform_surface.h"
        void *uniform;
        status = vk::Result(vmaMapMemory(m_allocator, m_uniformAllocation, &uniform));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "failed to map image memory!" << std::endl;
                return false;
        }
        memcpy(uniform, uniform_surface_qpqVGm, uniform_surface_qpqVGm_len);
        vmaUnmapMemory(m_allocator, m_uniformAllocation);

        vk::DescriptorBufferInfo descriptorBufferInfo;
        descriptorBufferInfo.setBuffer(m_uniformBuffer);
        descriptorBufferInfo.setRange(sizeof(glm::mat4));

        vk::WriteDescriptorSet writeInfo;
        writeInfo.setDescriptorCount(1);
        writeInfo.setDstSet(m_descriptorSet);
        writeInfo.setDescriptorType(vk::DescriptorType::eUniformBuffer);
        writeInfo.setPBufferInfo(&descriptorBufferInfo);
        m_logical.updateDescriptorSets(writeInfo, nullptr);

        return true;
}

Render::Device::~Device() { Shutdown(); }

} // namespace vkc