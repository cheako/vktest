/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "vulkanio.hpp"
#include "render/render.hpp"

#include <iostream>
#include <ctime>
#include <map>

namespace vkc
{

class VulkanIO::impl
{
      public:
        ~impl() { Shutdown(); };

        bool Init(VulkanIO *v)
        {
                m_this = v;
                return CreateInstance() && FindPhysicalDevice();
        }

        void Shutdown()
        {
                for (std::vector<Device>::iterator it = m_devices.begin(); it != m_devices.end(); ++it)
                        if (it->m_logical)
                                it->m_logical.destroy();

                if (m_this->instance)
                        m_this->instance.destroy();
        }

        void PollDisplays()
        {
                for (std::vector<Device>::iterator it = m_devices.begin(); it != m_devices.end(); ++it)
                {
#ifdef VKW_HAVE_VK_REGISTER_DEVICE_EVENT_EXT
                        vk::Result result = it->m_logical.getFenceStatus(it->m_deviceEventFence);
                        if (result == vk::Result::eSuccess)
                        {
                                it->m_logical.resetFences(1, &it->m_deviceEventFence);
                                it->EnumerateDisplays();
                        }
#else
                        static bool init = true;
                        if (init)
                        {
                                it->EnumerateDisplays();
                                init = false;
                        }
#endif
                }
        }

        bool IsValid()
        {
                static int a = 0;
                std::cout << "Ctr: " << a << std::endl;
                return a++ < 5;
        }

      private:
        VulkanIO *m_this;

        class Device
        {
              public:
                ~Device() { Shutdown(); }

                bool Init(vk::PhysicalDevice p, vk::Device l)
                {
                        m_physical = p;
                        m_logical = l;

#ifdef VKW_HAVE_VK_REGISTER_DEVICE_EVENT_EXT
                        vk::Result result;
                        vk::DeviceEventInfoEXT DeviceEventInfoEXT;
                        DeviceEventInfoEXT.setDeviceEvent(vk::DeviceEventTypeEXT::eDisplayHotplug);
                        std::tie(result, m_deviceEventFence) = m_logical.registerEventEXT(DeviceEventInfoEXT);
                        if (result != vk::Result::eSuccess)
                        {
                                std::cerr << "Failed to create Device Event Fence." << std::endl;
                                return false;
                        }
#endif
                        return true;
                }

                void Shutdown()
                {
                }

                class Display
                {
                      public:
                        bool Init(vk::DisplayPropertiesKHR p)
                        {
                                m_properties = p;
                                return true;
                        }

                        std::time_t atime;

                        vk::SurfaceKHR m_surface;

                      private:
                        vk::DisplayPropertiesKHR m_properties;
                };
                typedef std::map<vk::DisplayKHR, Display> Aa;
                Aa m_displays;

                vk::Device m_logical;
#ifdef VKW_HAVE_VK_REGISTER_DEVICE_EVENT_EXT
                vk::Fence m_deviceEventFence;
#endif

                void EnumerateDisplays()
                {
                        std::time_t now;
                        vk::Result result;
                        std::time(&now);
                        std::vector<vk::DisplayPropertiesKHR> properties;
                        std::tie(result, properties) = m_physical.getDisplayPropertiesKHR();
                        if (result != vk::Result::eSuccess)
                                return;
                        for (std::vector<vk::DisplayPropertiesKHR>::iterator it = properties.begin(); it != properties.end(); ++it)
                        {
                                if (0 == m_displays.count(it->display))
                                {
                                        auto item = std::pair<vk::DisplayKHR, Display>(it->display, Display());
                                        if (item.second.Init(*it))
                                        {
                                                m_displays.insert(item);
                                                singliton_render->InsertSurface(m_logical, item.second.m_surface);
                                                item.second.atime = now;
                                        }
                                }
                                else
                                {
                                        Display *d = &m_displays[it->display];
                                        d->atime = now;
                                }
                        }
                        for (Aa::iterator it = m_displays.begin(); it != m_displays.end(); ++it)
                        {
                                if (it->second.atime != now)
                                {
                                        singliton_render->RemoveSurface(m_logical, it->second.m_surface);
                                        m_displays.erase(it);
                                }
                        }
                }

              private:
                vk::PhysicalDevice m_physical;
        };
        std::vector<Device> m_devices;

        bool CreateInstance()
        {
                VulkanIO *v = m_this;
                std::vector<char const *> wantInstanceLayers;
                std::vector<char const *> wantInstanceExtensions{
                    VK_KHR_DISPLAY_EXTENSION_NAME,
                    VK_KHR_SURFACE_EXTENSION_NAME};

#ifndef NDEBUG
                wantInstanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
                wantInstanceLayers.push_back("VK_LAYER_LUNARG_standard_validation");
#endif

                std::vector<vk::LayerProperties> layers;
                std::tie(v->status, layers) = vk::enumerateInstanceLayerProperties();
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to enumerateInstanceLayerProperties." << std::endl;
                        return false;
                }

                std::vector<char const *> enabledInstanceLayers;
                for (std::vector<vk::LayerProperties>::const_iterator x = layers.begin(); x != layers.end(); ++x)
                        for (std::vector<char const *>::const_iterator y = wantInstanceLayers.begin(); y != wantInstanceLayers.end(); ++y)
                                if (0 == strcmp(x->layerName, *y))
                                        enabledInstanceLayers.push_back(*y);

                std::vector<vk::ExtensionProperties> extentions;
                std::tie(v->status, extentions) = vk::enumerateInstanceExtensionProperties();
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to enumerateInstanceExtensionProperties." << std::endl;
                        return false;
                }

                std::vector<char const *> enabledInstanceExtensions;
                for (std::vector<vk::ExtensionProperties>::const_iterator x = extentions.begin(); x != extentions.end(); ++x)
                        for (std::vector<char const *>::const_iterator y = wantInstanceExtensions.begin(); y != wantInstanceExtensions.end(); ++y)
                                if (0 == strcmp(x->extensionName, *y))
                                {
                                        enabledInstanceExtensions.push_back(*y);
                                        m_this->extentions.insert(*y);
                                }

#ifndef NDEBUG
                for (std::vector<char const *>::const_iterator i = enabledInstanceExtensions.begin(); i != enabledInstanceExtensions.end(); ++i)
                        std::cout << *i << std::endl;
#endif

                vk::ApplicationInfo appInfo;
                appInfo.setApiVersion(VK_API_VERSION_1_1);
                appInfo.setApplicationVersion(v->applicationVersion);
                appInfo.setPApplicationName(v->applicationName.c_str());
                appInfo.setPEngineName("VkWaylandFbCon");
                appInfo.setEngineVersion(VK_MAKE_VERSION(0, 0, 1));

                vk::InstanceCreateInfo instanceCreateInfo;
                instanceCreateInfo.setPApplicationInfo(&appInfo);
                instanceCreateInfo.setEnabledLayerCount(static_cast<uint32_t>(enabledInstanceLayers.size()));
                instanceCreateInfo.setPpEnabledLayerNames(enabledInstanceLayers.data());
                instanceCreateInfo.setEnabledExtensionCount(static_cast<uint32_t>(enabledInstanceExtensions.size()));
                instanceCreateInfo.setPpEnabledExtensionNames(enabledInstanceExtensions.data());

                std::tie(v->status, v->instance) = vk::createInstance(instanceCreateInfo);
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create Vulkan instance." << std::endl;
                        return false;
                }

                return true;
        }

        bool FindPhysicalDevice()
        {
                VulkanIO *v = m_this;
                std::vector<vk::PhysicalDevice> physicalDevices;
                std::tie(v->status, physicalDevices) = v->instance.enumeratePhysicalDevices();
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to find Vulkan capable devices." << std::endl;
                        return false;
                }

                for (auto physicalDevice : physicalDevices)
                {
                        auto const queueFamilyProperties = physicalDevice.getQueueFamilyProperties();

                        for (uint32_t i = 0; i < queueFamilyProperties.size(); ++i)
                        {
                                if ((queueFamilyProperties[i].queueFlags & vk::QueueFlagBits::eGraphics))
                                {
                                        CreateLogicalDevice(physicalDevice, i);
                                }
                        }
                }

                if (m_devices.empty())
                {
                        v->status = vk::Result::eErrorInitializationFailed;
                        std::cerr << "Failed to find discrete gpu." << std::endl;
                        return false;
                }
                return true;
        }

        void CreateLogicalDevice(vk::PhysicalDevice physical, uint32_t familyIndex)
        {
                VulkanIO *v = m_this;
                vk::DeviceQueueCreateInfo deviceQueueCreateInfo;
                float const priority = 1.0f;
                deviceQueueCreateInfo.setPQueuePriorities(&priority);
                deviceQueueCreateInfo.setQueueCount(1);
                deviceQueueCreateInfo.setQueueFamilyIndex(familyIndex);

                std::vector<char const *> wantExtensionNames = {
                    VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME,
                    VK_KHR_MAINTENANCE1_EXTENSION_NAME,
                    VK_KHR_SAMPLER_YCBCR_CONVERSION_EXTENSION_NAME,
                    VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME,
                    VK_KHR_IMAGE_FORMAT_LIST_EXTENSION_NAME,
                    VK_KHR_BIND_MEMORY_2_EXTENSION_NAME,
                    VK_EXT_IMAGE_DRM_FORMAT_MODIFIER_EXTENSION_NAME,
                    VK_KHR_DISPLAY_SWAPCHAIN_EXTENSION_NAME,
                    VK_EXT_DISPLAY_CONTROL_EXTENSION_NAME,
                    VK_KHR_SWAPCHAIN_EXTENSION_NAME};

                std::vector<vk::ExtensionProperties> extentions;
                std::tie(v->status, extentions) = physical.enumerateDeviceExtensionProperties();
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to enumerateDeviceExtensionProperties." << std::endl;
                        return;
                }

                std::set<std::string> device_extentions;
                std::vector<char const *> deviceExtensionNames;
                for (std::vector<vk::ExtensionProperties>::const_iterator x = extentions.begin(); x != extentions.end(); ++x)
                        for (std::vector<char const *>::const_iterator y = wantExtensionNames.begin(); y != wantExtensionNames.end(); ++y)
                                if (0 == strcmp(x->extensionName, *y))
                                {
                                        deviceExtensionNames.push_back(*y);
                                        device_extentions.insert(*y);
                                }

                vk::DeviceCreateInfo deviceCreateInfo;
                deviceCreateInfo.setQueueCreateInfoCount(1);
                deviceCreateInfo.setPQueueCreateInfos(&deviceQueueCreateInfo);
                deviceCreateInfo.setEnabledExtensionCount(deviceExtensionNames.size());
                deviceCreateInfo.setPpEnabledExtensionNames(deviceExtensionNames.data());

                vk::Device logical;
                std::tie(v->status, logical) = physical.createDevice(deviceCreateInfo);
                if (v->status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create logical device." << std::endl;
                }
                else
                {
                        Device device;
                        m_devices.insert(m_devices.end(), device);
                        device.Init(physical, logical);
                        singliton_render->CreateDevice(physical, logical, familyIndex, device_extentions);
                }
        }
};

VulkanIO::VulkanIO() : pImpl(std::make_unique<impl>())
{
}
VulkanIO::VulkanIO(const char *appName, uint32_t appVersion, std::vector<vk::PhysicalDeviceType> gpuTypes)
    : applicationName(appName), applicationVersion(appVersion), gpuTypes(gpuTypes), pImpl(std::make_unique<impl>())
{
}
VulkanIO::~VulkanIO() { Shutdown(); }

bool VulkanIO::Init() { return pImpl->Init(this); }

void VulkanIO::Shutdown()
{
}

void VulkanIO::PollDisplays()
{
        pImpl->PollDisplays();
}

bool VulkanIO::IsValid()
{
        return pImpl->IsValid();
}

} // namespace vkc